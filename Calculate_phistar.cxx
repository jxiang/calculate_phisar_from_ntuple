#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>
#include <TVector3.h>


//using namespace TMVA;
using namespace std;
using namespace chrono;

//bool sortcol( const vector<float>& v1,const vector<float>& v2 ) {
//    return v1[0] > v2[0];
//}



vector <string> glob(const char *pattern) {
    glob_t g;
    glob(pattern, GLOB_TILDE, nullptr, &g); // one should ensure glob returns 0!
    vector <string> filelist;
    filelist.reserve(g.gl_pathc);
    for (size_t i = 0; i < g.gl_pathc; ++i) {
        filelist.emplace_back(g.gl_pathv[i]);
    }
    globfree(&g);
    return filelist;
}

void ROOT_Swap_TLorentzVector(TLorentzVector& TLV_0, TLorentzVector& TLV_1 ){
    TLorentzVector TLV_Temp = TLV_0;
    TLV_0 = TLV_1;
    TLV_1 = TLV_Temp;
    return;
}




TVector3
CalculateImpactParamVec(float T_d0, float T_z0, float T_VX, float T_VY, float T_VZ, float T_Phi, float PV_X, float PV_Y,
                        float PV_Z) {

    TVector3 pionVector;
    TVector3 initialPoint;
    TVector3 primaryVertex;
    TVector3 IPVector;


    pionVector.SetXYZ(T_VX, T_VY, T_VZ);
    initialPoint.SetXYZ(-T_d0 * sin(T_Phi), T_d0 * cos(T_Phi), T_z0);
    primaryVertex.SetXYZ(PV_X, PV_Y, PV_Z);
    double k = (primaryVertex - initialPoint) * pionVector / pionVector.Mag2();
    IPVector = initialPoint + k * pionVector - primaryVertex;

    return IPVector;


}

float Cal_1p_Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged.Boost(boostIntoReferenceFrame);
        neutral.Boost(boostIntoReferenceFrame);
    }
    return (charged.E() - neutral.E()) / (charged.E() + neutral.E());
}

float Cal_xTauFW_Upsilon_a1(TLorentzVector pion, TLorentzVector intermediate_rho, const TLorentzVector &referenceFrame) {

    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        pion.Boost(boostIntoReferenceFrame);
        intermediate_rho.Boost(boostIntoReferenceFrame);
    }
    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);

    return y_a1;

}

float Cal_3p_rho0_Upsilon(TLorentzVector charged_same, TLorentzVector charged_oppo, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged_same.Boost(boostIntoReferenceFrame);
        charged_oppo.Boost(boostIntoReferenceFrame);
    }
    return (charged_same.E() - charged_oppo.E()) / (charged_same.E() + charged_oppo.E());
}


float Acoplanarity_RhoRho(TLorentzVector chargedPlus, TLorentzVector neutralPlus, TLorentzVector chargedMinus,
                                TLorentzVector neutralMinus, const TLorentzVector& referenceFrame) {
    // check that we do have valid charged and neutral 4 vectors
    //  if (chargedPlus.Mag() == 0 or neutralPlus.Mag() == 0 or chargedMinus.Mag() == 0 or neutralMinus.Mag() == 0) {
    //    return -5.;
    //  }
    // boost all 4-vectors into the reference frame
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    chargedPlus.Boost(boostIntoReferenceFrame);
    neutralPlus.Boost(boostIntoReferenceFrame);
    chargedMinus.Boost(boostIntoReferenceFrame);
    neutralMinus.Boost(boostIntoReferenceFrame);

    // parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallelPlus = (neutralPlus.Vect() * chargedPlus.Vect().Unit()) * chargedPlus.Vect().Unit();
    TVector3 neutralParallelMinus = (neutralMinus.Vect() * chargedMinus.Vect().Unit()) * chargedMinus.Vect().Unit();

    // perpendicular component of pi0
    TVector3 neutralPerpPlus = (neutralPlus.Vect() - neutralParallelPlus).Unit();
    TVector3 neutralPerpMinus = (neutralMinus.Vect() - neutralParallelMinus).Unit();

    float triplecorr =
        chargedMinus.Vect().Dot(neutralPerpPlus.Cross(neutralPerpMinus));  // the T-odd triple correlation \mathcal(O)^{*}_{CP}

    float phistar = TMath::ACos(neutralPerpPlus.Dot(neutralPerpMinus));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;

    // Do upsilon separation in xVarGroups.cxx
}


float Acoplanarity_IP(TLorentzVector piPlus, TLorentzVector ipVectorPlus, TLorentzVector piMinus, TLorentzVector ipVectorMinus,
                            const TLorentzVector& referenceFrame) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    piPlus.Boost(boostIntoReferenceFrame);
    piMinus.Boost(boostIntoReferenceFrame);
    ipVectorPlus.Boost(boostIntoReferenceFrame);
    ipVectorMinus.Boost(boostIntoReferenceFrame);
    TVector3 ip3VectorPlus = ipVectorPlus.Vect();
    TVector3 ip3VectorMinus = ipVectorMinus.Vect();
    // parallel projections of impact vector onto pion direction
    TVector3 ipVectorParallelPlus = (ip3VectorPlus * piPlus.Vect().Unit()) * piPlus.Vect().Unit();
    TVector3 ipVectorParallelMinus = (ip3VectorMinus * piMinus.Vect().Unit()) * piMinus.Vect().Unit();
    // perpendicular component of impact vector
    TVector3 ipVectorPerpPlus = (ip3VectorPlus - ipVectorParallelPlus).Unit();
    TVector3 ipVectorPerpMinus = (ip3VectorMinus - ipVectorParallelMinus).Unit();
    // Triple-odd correlation
    float triplecorr = piMinus.Vect().Unit().Dot(ipVectorPerpPlus.Cross(ipVectorPerpMinus));

    float phistar = TMath::ACos(ipVectorPerpPlus * ipVectorPerpMinus);
    if (triplecorr < 0) { phistar = TMath::TwoPi() - phistar; }

    return phistar;
}

float Acoplanarity_IP_rho(TLorentzVector track, TLorentzVector ipVector_track, TLorentzVector charged_rho, TLorentzVector neutral_rho,
                                const TLorentzVector& referenceFrame, bool track_is_plus) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    track.Boost(boostIntoReferenceFrame);
    ipVector_track.Boost(boostIntoReferenceFrame);
    charged_rho.Boost(boostIntoReferenceFrame);
    neutral_rho.Boost(boostIntoReferenceFrame);

    // IP vector
    TVector3 ip3Vector_track = ipVector_track.Vect();

    // parallel projection of impact vector onto track direction
    TVector3 ipVectorParallel_track = (ip3Vector_track * track.Vect().Unit()) * track.Vect().Unit();
    // Perpendicular component of impact vector
    TVector3 ipVectorPerp_track = (ip3Vector_track - ipVectorParallel_track).Unit();

    // Parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallel = (neutral_rho.Vect() * charged_rho.Vect().Unit()) * charged_rho.Vect().Unit();
    // Perpendicular component of pi0
    TVector3 neutralPerp = (neutral_rho.Vect() - neutralParallel).Unit();

    // the T-odd triple correlation (\mathcal(O)^{*}_{CP}) depends on the charge
    float triplecorr = track.Vect().Dot(neutralPerp.Cross(ipVectorPerp_track));
    if (track_is_plus) triplecorr = charged_rho.Vect().Dot(ipVectorPerp_track.Cross(neutralPerp));

    float phistar = TMath::ACos(neutralPerp.Dot(ipVectorPerp_track));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;

    return phistar;
}




TLorentzVector Retrieve_IP_from_track(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip){
    TVector3 Impact_parameter_From_ntuple;
    Impact_parameter_From_ntuple.SetXYZ(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);
    TLorentzVector TLV_IP = TLorentzVector(Impact_parameter_From_ntuple.Unit(),0); //need normalize
    return TLV_IP;
}



vector<TLorentzVector> Get_C_Closest(TLorentzVector tau_0_track0_p4,TLorentzVector tau_0_track1_p4,TLorentzVector tau_0_track2_p4,float tau_0_track0_q,float tau_0_track1_q,float tau_0_track2_q,float tau_0_q){

    vector<TLorentzVector> Components;


    TLorentzVector Pion_3p_OS_C;
    TLorentzVector rho_3p_OS_C;
    TLorentzVector Same_charge_OS_C;
    TLorentzVector Oppo_Charge_OS_C;


    if(tau_0_q*tau_0_track0_q<0){
        TLorentzVector ir_0 =tau_0_track0_p4 +tau_0_track1_p4;
        TLorentzVector ir_1 =tau_0_track0_p4 +tau_0_track2_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track2_p4;
            Same_charge_OS_C =tau_0_track1_p4;
            Oppo_Charge_OS_C =tau_0_track0_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track1_p4;
            Same_charge_OS_C =tau_0_track2_p4;
            Oppo_Charge_OS_C =tau_0_track0_p4;
        }

    }
    else if(tau_0_q*tau_0_track1_q<0){
        TLorentzVector ir_0 =tau_0_track1_p4 +tau_0_track0_p4;
        TLorentzVector ir_1 =tau_0_track1_p4 +tau_0_track2_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track2_p4;
            Same_charge_OS_C =tau_0_track0_p4;
            Oppo_Charge_OS_C =tau_0_track1_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track0_p4;
            Same_charge_OS_C =tau_0_track2_p4;
            Oppo_Charge_OS_C =tau_0_track1_p4;
        }
    }
    else if(tau_0_q*tau_0_track2_q<0){
        TLorentzVector ir_0 =tau_0_track2_p4 +tau_0_track0_p4;
        TLorentzVector ir_1 =tau_0_track2_p4 +tau_0_track1_p4;
        if(abs(ir_0.M()-0.775) < abs(ir_1.M()-0.775)){
            rho_3p_OS_C = ir_0;
            Pion_3p_OS_C =tau_0_track1_p4;
            Same_charge_OS_C =tau_0_track0_p4;
            Oppo_Charge_OS_C =tau_0_track2_p4;
        }
        else{
            rho_3p_OS_C = ir_1;
            Pion_3p_OS_C =tau_0_track0_p4;
            Same_charge_OS_C =tau_0_track1_p4;
            Oppo_Charge_OS_C =tau_0_track2_p4;
        }
    }
    else{
        cout<<"Error"<<endl;
    }


    Components.push_back(Same_charge_OS_C);
    Components.push_back(Oppo_Charge_OS_C);




}


vector<float> Calculate_phistar_1p1p(int tau_0_decay_mode,int tau_1_decay_mode,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,TVector3 primary_vertex_v,TVector3 beamSpot_v,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip){

    TLorentzVector reference_frame = tau_0_decay_charged_p4 + tau_0_decay_neutral_p4 + tau_1_decay_charged_p4 + tau_1_decay_neutral_p4;

    vector<float> Phistar_result;
    if(ditau_qxq>0){
        return Phistar_result;
    }

    if((tau_0_decay_mode == 1 || tau_0_decay_mode == 2 ) && (tau_1_decay_mode == 1 || tau_1_decay_mode == 2 )){ //1p1n-1p1n or 1pXn, I will not add specific shifting for 1pXn. Please add additional cut if needed.
        TLorentzVector Charged_plus = tau_0_decay_charged_p4;
        TLorentzVector Neutral_plus = tau_0_decay_neutral_p4;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector Neutral_minus = tau_1_decay_neutral_p4;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p1n_1p1n = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0y1 = Cal_1p_Upsilon(Charged_plus,Neutral_plus,reference_frame) *Cal_1p_Upsilon(Charged_minus,Neutral_minus,reference_frame);
        if (y0y1 < 0) {
            if (phistar_1p1n_1p1n > TMath::Pi())
                phistar_1p1n_1p1n -= TMath::Pi();
            else
                phistar_1p1n_1p1n += TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p1n_1p1n);
    }


    if((tau_0_decay_mode == 1 || tau_0_decay_mode == 2 ) && tau_1_decay_mode == 0){ //1p1n-1p0n or 1pXn-1p0n
        TLorentzVector tau1_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);
        bool tau1_is_positive = true;
        if(tau_1_q<0){
            tau1_is_positive = false;
        }

        float phistar_1p1n_1p0n = Acoplanarity_IP_rho(tau_1_decay_charged_p4,tau1_ip_retrieved,tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame,tau1_is_positive);

        float the_rho_part_upsilon = Cal_1p_Upsilon(tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame);
        if (the_rho_part_upsilon > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_1p1n_1p0n < TMath::Pi())
                phistar_1p1n_1p0n += TMath::Pi();
            else
                phistar_1p1n_1p0n -= TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p1n_1p0n);
    }


    if(tau_0_decay_mode == 0 && (tau_1_decay_mode == 1 || tau_1_decay_mode == 2 )){ //1p0n-1p1n or 1p0n-1pXn
        TLorentzVector tau0_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip);
        bool tau0_is_positive = true;
        if(tau_0_q<0){
            tau0_is_positive = false;
        }

        float phistar_1p0n_1p1n = Acoplanarity_IP_rho(tau_0_decay_charged_p4,tau0_ip_retrieved,tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame,tau0_is_positive);

        float the_rho_part_upsilon = Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);
        if (the_rho_part_upsilon > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_1p0n_1p1n < TMath::Pi())
                phistar_1p0n_1p1n += TMath::Pi();
            else
                phistar_1p0n_1p1n -= TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p0n_1p1n);


    }
    if(tau_0_decay_mode == 0 && tau_1_decay_mode == 0){
        TLorentzVector tau0_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip);
        TLorentzVector tau1_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);

        TLorentzVector Charged_plus = tau_0_decay_charged_p4;
        TLorentzVector IP_plus = tau0_ip_retrieved;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector IP_minus = tau1_ip_retrieved;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(IP_plus,IP_minus);
        }

        float phistar_1p0n_1p0n = Acoplanarity_IP(Charged_plus,IP_plus,Charged_minus,IP_minus,reference_frame);

        Phistar_result.push_back(phistar_1p0n_1p0n);

    }

    return Phistar_result;


}



vector<float> Calculate_phistar_3p1p(int tau_0_decay_mode,int tau_1_decay_mode,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,TVector3 primary_vertex_v,TVector3 beamSpot_v,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip,TLorentzVector tau_0_track0_p4,TLorentzVector tau_0_track1_p4,TLorentzVector tau_0_track2_p4,float tau_0_track0_q,float tau_0_track1_q,float tau_0_track2_q){

    TLorentzVector reference_frame = tau_0_decay_charged_p4 + tau_0_decay_neutral_p4 + tau_1_decay_charged_p4 + tau_1_decay_neutral_p4;

    vector<float> Phistar_result;
    if(ditau_qxq>0){
        return Phistar_result;
    }

    if(tau_0_decay_mode == 3 && tau_1_decay_mode == 1){ //3p1n-1p1n


        TLorentzVector LTV_3p_pion_lowest_pT = tau_0_track0_p4;
        TLorentzVector LTV_3p_rho_lowest_pT = tau_0_track1_p4+ tau_0_track2_p4;

        TLorentzVector Charged_plus = LTV_3p_pion_lowest_pT;
        TLorentzVector Neutral_plus = LTV_3p_rho_lowest_pT;
        TLorentzVector Charged_minus = tau_1_decay_charged_p4;
        TLorentzVector Neutral_minus = tau_1_decay_neutral_p4;


        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p3n_1p1n_lowest_pT = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0a1y1 = Cal_xTauFW_Upsilon_a1(LTV_3p_pion_lowest_pT,LTV_3p_rho_lowest_pT,reference_frame) *Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);
        if (y0a1y1 < 0) {
            if (phistar_1p3n_1p1n_lowest_pT > TMath::Pi())
                phistar_1p3n_1p1n_lowest_pT -= TMath::Pi();
            else
                phistar_1p3n_1p1n_lowest_pT += TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p3n_1p1n_lowest_pT); //3p0n-1p1n-lowest_pT-a1-method

        vector<TLorentzVector> TLV_rho0rho_components = Get_C_Closest(tau_0_track0_p4,tau_0_track1_p4,tau_0_track2_p4,tau_0_track0_q,tau_0_track1_q,tau_0_track2_q,tau_0_q);
        Charged_plus = TLV_rho0rho_components[0];
        Charged_minus = TLV_rho0rho_components[1];
        Charged_minus = tau_1_decay_charged_p4;
        Neutral_minus = tau_1_decay_neutral_p4;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(Neutral_plus,Neutral_minus);
        }
        float phistar_1p3n_1p1n_C_closest = Acoplanarity_RhoRho(Charged_plus, Neutral_plus, Charged_minus,Neutral_minus,reference_frame);
        float y0rho0y1 = Cal_3p_rho0_Upsilon(TLV_rho0rho_components[0],TLV_rho0rho_components[1],reference_frame) *Cal_1p_Upsilon(tau_1_decay_charged_p4,tau_1_decay_neutral_p4,reference_frame);
        if (y0rho0y1 < 0) {
            if (phistar_1p3n_1p1n_C_closest > TMath::Pi())
                phistar_1p3n_1p1n_C_closest -= TMath::Pi();
            else
                phistar_1p3n_1p1n_C_closest += TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p3n_1p1n_C_closest);//3p0n-1p1n-Oppo_charged_closest_mass_rho0-method







    }


    return Phistar_result;


}

bool Check_lepton_channel(int tau_1_matched_pdgId){
    if(abs(tau_1_matched_pdgId)==11 || abs(tau_1_matched_pdgId)==13){
        return true;
    }
    return false;
}


vector<float> Calculate_phistar_1plep(int tau_0_decay_mode,int tau_1_matched_pdgId,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,TVector3 primary_vertex_v,TVector3 beamSpot_v,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip,TLorentzVector tau_1_p4,float tau_1_trk_d0,float tau_1_trk_z0){

    vector<float> Phistar_result;
    if(ditau_qxq>0){
        return Phistar_result;
    }

    if(!(Check_lepton_channel(tau_1_matched_pdgId))){
        return Phistar_result;
    }

    TVector3 Track_Direction = tau_1_p4->Vect();
    TVector3 Impact_vector_lepton = CalculateImpactParamVec(tau_1_trk_d0,tau_1_trk_z0,Track_Direction.X(),Track_Direction.Y(),Track_Direction.Z(),tau_1_p4.Phi(),(primary_vertex_v.X() - (-0.5)),(primary_vertex_v.Y()-(-0.5)),(primary_vertex_v.Z()-(0)));
    TLorentzVector Lepton_ip = TLorentzVector(Impact_vector_lepton.Unit(),0); //need normalize
    TLorentzVector reference_frame = tau_1_p4 + tau_0_decay_charged_p4 + tau_0_decay_neutral_p4;

    if(tau_0_decay_mode == 1 || tau_0_decay_mode == 2){

        bool tau1_is_positive = true;
        if(tau_1_q<0){
            tau1_is_positive = false;
        }

        float phistar_1p1n_lep = Acoplanarity_IP_rho(tau_1_p4,Lepton_ip,tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame,tau1_is_positive);

        float the_rho_part_upsilon = Cal_1p_Upsilon(tau_0_decay_charged_p4,tau_0_decay_neutral_p4,reference_frame);
        if (the_rho_part_upsilon > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_1p1n_lep < TMath::Pi())
                phistar_1p1n_lep += TMath::Pi();
            else
                phistar_1p1n_lep -= TMath::Pi();
        }
        if (phistar_1p1n_lep < TMath::Pi())
            phistar_1p1n_lep += TMath::Pi();
        else
            phistar_1p1n_lep -= TMath::Pi();


        if(tau_0_decay_mode == 2){
            bool Follow_xTauFW = true;
            //https://gitlab.cern.ch/ATauLeptonAnalysiS/xTauFramework/-/blob/master/source/xHCP/Root/xHCP.cxx#L389
            //In the xTauFW code, the phistar is shifted in 1pXn channel, but the description is for 3-prong channel.
            //Please change Follow_xTauFW to "False" if it is not for 1pXn channel.
            if(Follow_xTauFW){
                if (phistar_1p1n_lep < TMath::Pi())
                    phistar_1p1n_lep += TMath::Pi();
                else
                    phistar_1p1n_lep -= TMath::Pi();
            }


        }

        Phistar_result.push_back(phistar_1p1n_lep);

    }


    if(tau_0_decay_mode == 0){
        TLorentzVector tau0_ip_retrieved = Retrieve_IP_from_track(ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip);


        TLorentzVector Charged_plus = tau_0_decay_charged_p4;
        TLorentzVector IP_plus = tau0_ip_retrieved;
        TLorentzVector Charged_minus = tau_1_p4;
        TLorentzVector IP_minus = Lepton_ip;
        if(tau_1_q>0){
            ROOT_Swap_TLorentzVector(Charged_plus,Charged_minus);
            ROOT_Swap_TLorentzVector(IP_plus,IP_minus);
        }

        float phistar_1p0n_lep = Acoplanarity_IP(Charged_plus,IP_plus,Charged_minus,IP_minus,reference_frame);
        if (phistar_1p0n_lep < TMath::Pi())
            phistar_1p0n_lep += TMath::Pi();
        else
            phistar_1p0n_lep -= TMath::Pi();
        Phistar_result.push_back(phistar_1p0n_lep);

    }








    return Phistar_result;

}

vector<float> Calculate_phistar_3plep(int tau_0_decay_mode,int tau_1_matched_pdgId,float ditau_qxq,float tau_0_q,float tau_1_q,TLorentzVector tau_0_decay_charged_p4,TLorentzVector tau_0_decay_neutral_p4,TLorentzVector tau_1_decay_charged_p4,TLorentzVector tau_1_decay_neutral_p4,TVector3 primary_vertex_v,TVector3 beamSpot_v,float ditau_CP_ip_tau0_x_ip,float ditau_CP_ip_tau0_y_ip,float ditau_CP_ip_tau0_z_ip,float ditau_CP_ip_tau1_x_ip,float ditau_CP_ip_tau1_y_ip,float ditau_CP_ip_tau1_z_ip,TLorentzVector tau_0_track0_p4,TLorentzVector tau_0_track1_p4,TLorentzVector tau_0_track2_p4,float tau_0_track0_q,float tau_0_track1_q,float tau_0_track2_q,TLorentzVector tau_1_p4,float tau_1_trk_d0,float tau_1_trk_z0){

    vector<float> Phistar_result;
    if(ditau_qxq>0){
        return Phistar_result;
    }

    if(!(Check_lepton_channel(tau_1_matched_pdgId))){
        return Phistar_result;
    }

    TVector3 Track_Direction = tau_1_p4->Vect();
    TVector3 Impact_vector_lepton = CalculateImpactParamVec(tau_1_trk_d0,tau_1_trk_z0,Track_Direction.X(),Track_Direction.Y(),Track_Direction.Z(),tau_1_p4.Phi(),(primary_vertex_v.X() - (-0.5)),(primary_vertex_v.Y()-(-0.5)),(primary_vertex_v.Z()-(0)));
    TLorentzVector Lepton_ip = TLorentzVector(Impact_vector_lepton.Unit(),0); //need normalize
    TLorentzVector reference_frame = tau_1_p4 + tau_0_track0_p4 + tau_0_track1_p4 + tau_0_track2_p4 ;



    if(tau_0_decay_mode == 3){ //3p1n-1p1n


        TLorentzVector LTV_3p_pion_lowest_pT = tau_0_track0_p4;
        TLorentzVector LTV_3p_rho_lowest_pT = tau_0_track1_p4+ tau_0_track2_p4;


        bool tau1_is_positive = true;
        if(tau_1_q<0){
            tau1_is_positive = false;
        }

        float phistar_3p0n_lep_lowest_pT = Acoplanarity_IP_rho(tau_1_p4,Lepton_ip,LTV_3p_pion_lowest_pT,LTV_3p_rho_lowest_pT,reference_frame,tau1_is_positive);

        float y0a1 = Cal_xTauFW_Upsilon_a1(LTV_3p_pion_lowest_pT,LTV_3p_rho_lowest_pT,reference_frame);
        if (y0a1 > 0) { /* OK */
        } else {
            // Add +/- pi
            if (phistar_3p0n_lep_lowest_pT < TMath::Pi())
                phistar_3p0n_lep_lowest_pT += TMath::Pi();
            else
                phistar_3p0n_lep_lowest_pT -= TMath::Pi();
        }
        if (phistar_3p0n_lep_lowest_pT < TMath::Pi())
            phistar_3p0n_lep_lowest_pT += TMath::Pi();
        else
            phistar_3p0n_lep_lowest_pT -= TMath::Pi();

        Phistar_result.push_back(phistar_3p0n_lep_lowest_pT);//3p0n-lep-lowest_pT-a1-method


        vector<TLorentzVector> TLV_rho0rho_components = Get_C_Closest(tau_0_track0_p4,tau_0_track1_p4,tau_0_track2_p4,tau_0_track0_q,tau_0_track1_q,tau_0_track2_q,tau_0_q);
        Charged_Same = TLV_rho0rho_components[0];
        Charged_Oppo = TLV_rho0rho_components[1];

        float phistar_1p3n_lep_C_closest = Acoplanarity_IP_rho(tau_1_p4,Lepton_ip,Charged_Same,Charged_Oppo,reference_frame,tau1_is_positive);
        float y0rho0 = Cal_3p_rho0_Upsilon(Charged_Same,Charged_Oppo,reference_frame);
        if (y0rho0 < 0) {
            if (phistar_1p3n_lep_C_closest > TMath::Pi())
                phistar_1p3n_lep_C_closest -= TMath::Pi();
            else
                phistar_1p3n_lep_C_closest += TMath::Pi();
        }
        Phistar_result.push_back(phistar_1p3n_lep_C_closest);//3p0n-lep-Oppo_charged_closest_mass_rho0-method







    }




}








int Cal_Phi_star() {

    TChain *chain_Calculate_MC = new TChain("NOMINAL");
    TString Final_Name = "./Plots/";
    SetAtlasStyle();

    for (const auto &filename : glob(
        "../../../../CERN_BOX_SYN/Test_New_Branches/Charge_HH_w_121/VBF/user.jxiang.Htt_hh_V03.mc16_13TeV.346573.PoPy8_NNPDF30_VBFH125_tth30h20_unpol.D3.e7639_s3126_r10201_p3978.smPre_w_121_HS/user.jxiang.24590408._000001.HSM_common.root")) {
        chain_Calculate_MC->Add(filename.c_str());
    }








    UInt_t tau_0_decay_mode;
    UInt_t tau_1_decay_mode;
    Float_t ditau_qxq;
    Float_t tau_0_q;
    Float_t tau_1_q;
    Float_t tau_1_trk_d0;
    Float_t tau_1_trk_z0;
    Int_t tau_1_matched_pdgId;
//    TVector3 *beamSpot_v = new TVector3;
    TVector3 *primary_vertex_v = new TVector3;
    TLorentzVector *tau_0_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4 = new TLorentzVector;
    TLorentzVector *tau_1_p4 = new TLorentzVector;
    Double_t tauspinner_HCP_Theta_0;
    Double_t tauspinner_HCP_Theta_10;
    Double_t tauspinner_HCP_Theta_100;
    Double_t tauspinner_HCP_Theta_110;
    Double_t tauspinner_HCP_Theta_120;
    Double_t tauspinner_HCP_Theta_130;
    Double_t tauspinner_HCP_Theta_140;
    Double_t tauspinner_HCP_Theta_150;
    Double_t tauspinner_HCP_Theta_160;
    Double_t tauspinner_HCP_Theta_170;
    Double_t tauspinner_HCP_Theta_20;
    Double_t tauspinner_HCP_Theta_30;
    Double_t tauspinner_HCP_Theta_40;
    Double_t tauspinner_HCP_Theta_50;
    Double_t tauspinner_HCP_Theta_60;
    Double_t tauspinner_HCP_Theta_70;
    Double_t tauspinner_HCP_Theta_80;
    Double_t tauspinner_HCP_Theta_90;

    Float_t tau_0_track0_q;
    Float_t tau_0_track1_q;
    Float_t tau_0_track2_q;

    Float_t         ditau_CP_tau0_upsilon;
    Float_t         ditau_CP_tau1_upsilon;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_tau0_upsilon", &ditau_CP_tau0_upsilon);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_tau1_upsilon", &ditau_CP_tau1_upsilon);

    TLorentzVector *tau_0_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_0_decay_neutral_p4 = new TLorentzVector;
    chain_Calculate_MC->SetBranchAddress("tau_0_decay_charged_p4", &tau_0_decay_charged_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_decay_neutral_p4", &tau_0_decay_neutral_p4);



    TLorentzVector *tau_1_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4 = new TLorentzVector;
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_charged_p4", &tau_1_decay_charged_p4);
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4);


    Double_t tau_0_leadTrk_d0;
    Double_t tau_0_leadTrk_z0;
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_d0", &tau_0_leadTrk_d0);
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_z0", &tau_0_leadTrk_z0);

    TVector3 *tau_0_leadTrk_vertex_v = new TVector3;
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_vertex_v", &tau_0_leadTrk_vertex_v);

    Float_t         ditau_CP_ip_tau0_x_ip;
    Float_t         ditau_CP_ip_tau0_y_ip;
    Float_t         ditau_CP_ip_tau0_z_ip;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_x_ip", &ditau_CP_ip_tau0_x_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_y_ip", &ditau_CP_ip_tau0_y_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_z_ip", &ditau_CP_ip_tau0_z_ip);


    Float_t         ditau_CP_ip_tau1_x_ip;
    Float_t         ditau_CP_ip_tau1_y_ip;
    Float_t         ditau_CP_ip_tau1_z_ip;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_x_ip", &ditau_CP_ip_tau1_x_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_y_ip", &ditau_CP_ip_tau1_y_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_z_ip", &ditau_CP_ip_tau1_z_ip);


    Float_t ditau_CP_phi_star_cp_ip_ip;
    Float_t ditau_CP_phi_star_cp_ip_rho;
    Float_t ditau_CP_phi_star_cp_rho_ip;
    Float_t ditau_CP_phi_star_cp_rho_rho;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_ip_ip", &ditau_CP_phi_star_cp_ip_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_ip_rho", &ditau_CP_phi_star_cp_ip_rho);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_rho_ip", &ditau_CP_phi_star_cp_rho_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_rho_rho", &ditau_CP_phi_star_cp_rho_rho);

    chain_Calculate_MC->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode);
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_mode", &tau_1_decay_mode);
    chain_Calculate_MC->SetBranchAddress("ditau_qxq", &ditau_qxq);
    chain_Calculate_MC->SetBranchAddress("tau_0_q", &tau_0_q);
    chain_Calculate_MC->SetBranchAddress("tau_1_q", &tau_1_q);

    chain_Calculate_MC->SetBranchAddress("tau_1_matched_pdgId", &tau_1_matched_pdgId);
//    chain_Calculate_MC->SetBranchAddress("beamSpot_v", &beamSpot_v);
    chain_Calculate_MC->SetBranchAddress("primary_vertex_v", &primary_vertex_v);
    chain_Calculate_MC->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4);

    Float_t tau_1_trk_d0;
    Float_t tau_1_trk_z0;
    Int_t tau_1_matched_pdgId;
    chain_Calculate_MC->SetBranchAddress("tau_1_trk_d0", &tau_1_trk_d0);
    chain_Calculate_MC->SetBranchAddress("tau_1_trk_z0", &tau_1_trk_z0);
    chain_Calculate_MC->SetBranchAddress("tau_1_matched_pdgId", &tau_1_matched_pdgId);

    chain_Calculate_MC->SetBranchAddress("tau_1_p4", &tau_1_p4);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_0", &tauspinner_HCP_Theta_0);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_10", &tauspinner_HCP_Theta_10);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_100", &tauspinner_HCP_Theta_100);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_110", &tauspinner_HCP_Theta_110);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_120", &tauspinner_HCP_Theta_120);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_130", &tauspinner_HCP_Theta_130);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_140", &tauspinner_HCP_Theta_140);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_150", &tauspinner_HCP_Theta_150);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_160", &tauspinner_HCP_Theta_160);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_170", &tauspinner_HCP_Theta_170);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_20", &tauspinner_HCP_Theta_20);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_30", &tauspinner_HCP_Theta_30);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_40", &tauspinner_HCP_Theta_40);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_50", &tauspinner_HCP_Theta_50);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_60", &tauspinner_HCP_Theta_60);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_70", &tauspinner_HCP_Theta_70);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_80", &tauspinner_HCP_Theta_80);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_90", &tauspinner_HCP_Theta_90);

//    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_a1_ip", &ditau_CP_phi_star_cp_a1_ip);
//    chain_Calculate_MC->SetBranchAddress("ditau_CP_upsilon_a1_lep_3p0n", &ditau_CP_upsilon_a1_lep_3p0n);

    TH1F *MC_Phi_star_HH1p_rhorho_HH_Even_xTauFW = new TH1F("MC_Phi_star_HH1p_rhorho_HH_Even_xTauFW", "", 5, 0, 6.3);
    TH1F *MC_Phi_star_HH1p_rhorho_HH_Odd_xTauFW = new TH1F("MC_Phi_star_HH1p_rhorho_HH_Odd_xTauFW", "", 5, 0, 6.3);

    TH1F *MC_Phi_star_HH1p_rhorho_HH_Even_xTauFW_ntuple = new TH1F("MC_Phi_star_HH1p_rhorho_HH_Even_xTauFW_ntuple", "", 5, 0, 6.3);
    TH1F *MC_Phi_star_HH1p_rhorho_HH_Odd_xTauFW_ntuple = new TH1F("MC_Phi_star_HH1p_rhorho_HH_Odd_xTauFW_ntuple", "", 5, 0, 6.3);

    TH1F *upsilon_rhorho = new TH1F("upsilon_rhorho", "", 20, -1,1);
    TH1F *upsilon_rhorho_ntuple = new TH1F("upsilon_rhorho_ntuple", "", 20, -1,1);



    float Even_Weight_ipip_HL = 0;
    float Odd_Weight_ipip_HL = 0;


    int num_MC = 0;
    int num_Data = 0;


    int num_selection = 0;
    int limit063 = 0;
    TVector3 *beamSpot_v = new TVector3;
    beamSpot_v.SetXYZ(-0.5,-0.5,0);


    Int_t nentries = (Int_t) chain_Calculate_MC->GetEntries();
    for (Int_t i = 0; i < nentries; i++) {
        chain_Calculate_MC->GetEntry(i);

        vector<float> Calculated_phistar_1p1p;
        vector<float> Calculated_phistar_3p1p;
        vector<float> Calculated_phistar_1plep;
        vector<float> Calculated_phistar_3plep;

        cout<<"Calculated_phistar_1p1p: phistars in 1p1p channels"<<endl;
        cout<<"Calculated_phistar_3p1p: phistars in 3p0n-1p1n channel"<<endl;
        cout<<"Calculated_phistar_1plep: phistars in 1p-lep channel"<<endl;
        cout<<"Calculated_phistar_3plep: phistars in 3p0n-lep channel"<<endl;

        Calculated_phistar_1p1p  = Calculate_phistar_1p1p(tau_0_decay_mode,tau_1_decay_mode,ditau_qxq,tau_0_q,tau_1_q,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,*tau_1_decay_charged_p4,*tau_1_decay_neutral_p4,*primary_vertex_v,*beamSpot_v,ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip,ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip);
        Calculated_phistar_3p1p  = Calculate_phistar_3p1p(tau_0_decay_mode,tau_1_decay_mode,ditau_qxq,tau_0_q,tau_1_q,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,*tau_1_decay_charged_p4,*tau_1_decay_neutral_p4,*primary_vertex_v,*beamSpot_v,ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip,ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip,*tau_0_track0_p4,*tau_0_track1_p4,*tau_0_track2_p4,tau_0_track0_q,tau_0_track1_q,tau_0_track2_q);

        if(tau_1_is_lepton == false){
            continue;
        }
        Calculated_phistar_1plep = Calculate_phistar_1plep(tau_0_decay_mode,tau_1_matched_pdgId,ditau_qxq,tau_0_q,tau_1_q,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,*tau_1_decay_charged_p4,*tau_1_decay_neutral_p4,*primary_vertex_v,*beamSpot_v,ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip,ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip,*tau_1_p4,tau_1_trk_d0,tau_1_trk_z0);
        Calculated_phistar_3plep = Calculate_phistar_3plep(tau_0_decay_mode,tau_1_matched_pdgId,ditau_qxq,tau_0_q,tau_1_q,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,*tau_1_decay_charged_p4,*tau_1_decay_neutral_p4,*primary_vertex_v,*beamSpot_v,ditau_CP_ip_tau0_x_ip,ditau_CP_ip_tau0_y_ip,ditau_CP_ip_tau0_z_ip,ditau_CP_ip_tau1_x_ip,ditau_CP_ip_tau1_y_ip,ditau_CP_ip_tau1_z_ip,*tau_0_track0_p4,*tau_0_track1_p4,*tau_0_track2_p4,tau_0_track0_q,tau_0_track1_q,tau_0_track2_q,*tau_1_p4,tau_1_trk_d0,tau_1_trk_z0);





    }

    return 0;

}


void Calculate_phistar() {
    Cal_Phi_star();
    return;
}

