#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <TRandom3.h>

//using namespace TMVA;
using namespace std;
using namespace chrono;


//bool sortcol( const vector<float>& v1,const vector<float>& v2 ) {
//    return v1[0] > v2[0];
//}



vector <string> glob(const char *pattern) {
    glob_t g;
    glob(pattern, GLOB_TILDE, nullptr, &g); // one should ensure glob returns 0!
    vector <string> filelist;
    filelist.reserve(g.gl_pathc);
    for (size_t i = 0; i < g.gl_pathc; ++i) {
        filelist.emplace_back(g.gl_pathv[i]);
    }
    globfree(&g);
    return filelist;
}


int Preselection_Check(int Trigger_0, int Trigger_1, int Track_num_0, int Track_num_1, float Charge_0, float Charge_1,
                       float Charge_Product) {

    if ((Trigger_0 != 0 && (Track_num_0 == 1 || Track_num_0 == 3) && abs(Charge_0) == 1) &&
        (Trigger_1 != 0 && (Track_num_1 == 1 || Track_num_1 == 3) && abs(Charge_1) == 1) && Charge_Product < 0) {
        return 1;
    } else {
        return 0;
    }
}


float Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector &referenceFrame) {

    TLorentzVector Charged_r = charged;
    TLorentzVector Neutral_r = neutral;

    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        Charged_r.Boost(boostIntoReferenceFrame);
        Neutral_r.Boost(boostIntoReferenceFrame);
    }
    return (Charged_r.E() - Neutral_r.E()) / (Charged_r.E() + Neutral_r.E());
}


float Cal_phi_Star_RhoRho(const TLorentzVector Charged_plus, const TLorentzVector Neutral_plus,
                          const TLorentzVector Charged_minus, const TLorentzVector Neutral_minus,
                          const TLorentzVector Reference_Frame) {


    TLorentzVector Charged_plus_TBB = Charged_plus;
    TLorentzVector Neutral_plus_TBB = Neutral_plus;
    TLorentzVector Charged_minus_TBB = Charged_minus;
    TLorentzVector Neutral_minus_TBB = Neutral_minus;

    TLorentzVector referenceFrame = Reference_Frame;
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    Charged_plus_TBB.Boost(boostIntoReferenceFrame);
    Neutral_plus_TBB.Boost(boostIntoReferenceFrame);
    Charged_minus_TBB.Boost(boostIntoReferenceFrame);
    Neutral_minus_TBB.Boost(boostIntoReferenceFrame);

    // parallel projections of pi0 onto pi+/- direction
    TVector3 neutralParallelPlus =
        (Neutral_plus_TBB.Vect() * Charged_plus_TBB.Vect().Unit()) * Charged_plus_TBB.Vect().Unit();
    TVector3 neutralParallelMinus =
        (Neutral_minus_TBB.Vect() * Charged_minus_TBB.Vect().Unit()) * Charged_minus_TBB.Vect().Unit();

    // perpendicular component of pi0
    TVector3 neutralPerpPlus = (Neutral_plus_TBB.Vect() - neutralParallelPlus).Unit();
    TVector3 neutralPerpMinus = (Neutral_minus_TBB.Vect() - neutralParallelMinus).Unit();

    float triplecorr =
        Charged_minus_TBB.Vect().Dot(
            neutralPerpPlus.Cross(neutralPerpMinus));  // the T-odd triple correlation \mathcal(O)^{*}_{CP}

    float phistar = TMath::ACos(neutralPerpPlus.Dot(neutralPerpMinus));
    if (triplecorr < 0) phistar = 2 * TMath::Pi() - phistar;


    return phistar;
}


float
Cal_rhorho_Upsilon(const TLorentzVector charged, const TLorentzVector neutral, const TLorentzVector referenceFrame) {
    // only boost if we can boost into the reference frame
    TLorentzVector Charged_TBB = charged;
    TLorentzVector Neutral_TBB = neutral;

    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        Charged_TBB.Boost(boostIntoReferenceFrame);
        Neutral_TBB.Boost(boostIntoReferenceFrame);
    }
    return (Charged_TBB.E() - Neutral_TBB.E()) / (Charged_TBB.E() + Neutral_TBB.E());
}


float Calculated_Phi_Star_rhorho_Original(const TLorentzVector pi_C_0, const TLorentzVector pi_N_0,
                                          const TLorentzVector pi_C_1, const TLorentzVector pi_N_1,
                                          float charge_tau_0) {


    TLorentzVector Reference_Frame_Tautau = pi_C_0 + pi_N_0 + pi_C_1 + pi_N_1;
//    float Upsilon_rho = Upsilon(OtherTau_Charged,OtherTau_Neutral,Reference_Frame_Tautau);







    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;
    float Phi_Star_a1_rho_wrong;

    if (charge_tau_0 > 0.1) {

        Char_Plus = pi_C_0;
        Neut_Plus = pi_N_0;
        Char_Minus = pi_C_1;
        Neut_Minus = pi_N_1;


    } else if (charge_tau_0 < -0.1) {
        Char_Plus = pi_C_1;
        Neut_Plus = pi_N_1;
        Char_Minus = pi_C_0;
        Neut_Minus = pi_N_0;
    } else {
        return -10;
    }


    float phi_star_CP = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus, Char_Minus, Neut_Minus, Reference_Frame_Tautau);


    // Upsilon separation



    float y0y1 = Cal_rhorho_Upsilon(Char_Plus, Neut_Plus, Reference_Frame_Tautau) *
                 Cal_rhorho_Upsilon(Char_Minus, Neut_Minus, Reference_Frame_Tautau);

    if (y0y1 < 0) {
        if (phi_star_CP > TMath::Pi())
            phi_star_CP -= TMath::Pi();
        else
            phi_star_CP += TMath::Pi();
    }

    return phi_star_CP;


}


float Cal_sigma_based_on_pT(float v_a, float v_b, float v_c, float pT) {

    float Output_sigma;

    float SQRT_pT = sqrt(pT);

    Output_sigma = sqrt((v_a / SQRT_pT) * (v_a / SQRT_pT) + (v_b / pT) * (v_b / pT) + v_c * v_c);


    return Output_sigma;


}


TLorentzVector Smear_The_Neutral_Pion_Plus(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() + Smear_para * (D_phi / SQ_phi_eta) - Smear_perp * (D_eta / SQ_phi_eta);
    float Neut_Smear_Eta = pi_N.Eta() + Smear_para * (D_eta / SQ_phi_eta) + Smear_perp * (D_phi / SQ_phi_eta);


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


TLorentzVector Smear_The_Neutral_Pion_Plus_para(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() + Smear_para * (D_phi / SQ_phi_eta);
    float Neut_Smear_Eta = pi_N.Eta() + Smear_para * (D_eta / SQ_phi_eta);


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


TLorentzVector Smear_The_Neutral_Pion_Plus_perp(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() - Smear_perp * (D_eta / SQ_phi_eta);
    float Neut_Smear_Eta = pi_N.Eta() + Smear_perp * (D_phi / SQ_phi_eta);


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


TLorentzVector Smear_The_Neutral_Pion_Minus(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() - (Smear_para * (D_phi / SQ_phi_eta) - Smear_perp * (D_eta / SQ_phi_eta));
    float Neut_Smear_Eta = pi_N.Eta() - (Smear_para * (D_eta / SQ_phi_eta) + Smear_perp * (D_phi / SQ_phi_eta));


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


TLorentzVector Smear_The_Neutral_Pion_Minus_para(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() - (Smear_para * (D_phi / SQ_phi_eta));
    float Neut_Smear_Eta = pi_N.Eta() - (Smear_para * (D_eta / SQ_phi_eta));


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


TLorentzVector Smear_The_Neutral_Pion_Minus_perp(const TLorentzVector pi_C, const TLorentzVector pi_N) {

    TLorentzVector Charged_pion = pi_C;
    TLorentzVector Neutral_pion = pi_N;


    float pT_of_Neutral = pi_N.Pt();


    float para_a = (1.95) * (0.01);
    float para_b = (8.49) * (0.01);
    float para_c = (4.19) * (0.001);

    float perp_a = (1.70) * (0.01);
    float perp_b = (2.46) * (0.01);
    float perp_c = (3.40) * (0.001);

    float Calculate_Sigma_para = 0.173 * Cal_sigma_based_on_pT(para_a, para_b, para_c, pT_of_Neutral);
    float Calculate_Sigma_perp = 0.292 * Cal_sigma_based_on_pT(perp_a, perp_b, perp_c, pT_of_Neutral);

    float Smear_para = gRandom->Gaus(0, Calculate_Sigma_para);
    float Smear_perp = gRandom->Gaus(0, Calculate_Sigma_perp);

    float D_phi = pi_C.Phi() - pi_N.Phi();
    float D_eta = pi_C.Eta() - pi_N.Eta();
    float SQ_phi_eta = sqrt(D_phi * D_phi + D_eta * D_eta);


    float Neut_Smear_Phi = pi_N.Phi() - (-Smear_perp * (D_eta / SQ_phi_eta));
    float Neut_Smear_Eta = pi_N.Eta() - (Smear_perp * (D_phi / SQ_phi_eta));


    float Neut_Smear_M = pi_N.M();
    float Neut_Smear_pT = pi_N.Pt();

    TLorentzVector Neut_Smear;

    Neut_Smear.SetPtEtaPhiM(Neut_Smear_pT, Neut_Smear_Eta, Neut_Smear_Phi, Neut_Smear_M);
    return Neut_Smear;


}


float Calculated_Phi_Star_rhorho_Smearing(const TLorentzVector pi_C_0, const TLorentzVector pi_N_0,
                                          const TLorentzVector pi_C_1, const TLorentzVector pi_N_1, float charge_tau_0,
                                          int smear_plus) {


    TLorentzVector Reference_Frame_Tautau = pi_C_0 + pi_N_0 + pi_C_1 + pi_N_1;
//    float Upsilon_rho = Upsilon(OtherTau_Charged,OtherTau_Neutral,Reference_Frame_Tautau);







    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;

    if (charge_tau_0 > 0.1) {

        Char_Plus = pi_C_0;
        Neut_Plus = pi_N_0;
        Char_Minus = pi_C_1;
        Neut_Minus = pi_N_1;


    } else if (charge_tau_0 < -0.1) {
        Char_Plus = pi_C_1;
        Neut_Plus = pi_N_1;
        Char_Minus = pi_C_0;
        Neut_Minus = pi_N_0;
    } else {
        return -10;
    }


    TLorentzVector Neut_Plus_Smear;
    TLorentzVector Neut_Minus_Smear;


    if (smear_plus == 1) {

        Neut_Plus_Smear = Smear_The_Neutral_Pion_Plus(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Plus(Char_Minus, Neut_Minus);

    } else if (smear_plus == -1) {
        Neut_Plus_Smear = Smear_The_Neutral_Pion_Minus(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Minus(Char_Minus, Neut_Minus);
    }


    float phi_star_CP = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus_Smear, Char_Minus, Neut_Minus_Smear,
                                            Reference_Frame_Tautau);


    // Upsilon separation



    float y0y1 = Cal_rhorho_Upsilon(Char_Plus, Neut_Plus, Reference_Frame_Tautau) *
                 Cal_rhorho_Upsilon(Char_Minus, Neut_Minus, Reference_Frame_Tautau);

    if (y0y1 < 0) {
        if (phi_star_CP > TMath::Pi())
            phi_star_CP -= TMath::Pi();
        else
            phi_star_CP += TMath::Pi();
    }

    return phi_star_CP;


}


float Calculated_Phi_Star_rhorho_Smearing_para(const TLorentzVector pi_C_0, const TLorentzVector pi_N_0,
                                               const TLorentzVector pi_C_1, const TLorentzVector pi_N_1,
                                               float charge_tau_0, int smear_plus) {


    TLorentzVector Reference_Frame_Tautau = pi_C_0 + pi_N_0 + pi_C_1 + pi_N_1;
//    float Upsilon_rho = Upsilon(OtherTau_Charged,OtherTau_Neutral,Reference_Frame_Tautau);







    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;

    if (charge_tau_0 > 0.1) {

        Char_Plus = pi_C_0;
        Neut_Plus = pi_N_0;
        Char_Minus = pi_C_1;
        Neut_Minus = pi_N_1;


    } else if (charge_tau_0 < -0.1) {
        Char_Plus = pi_C_1;
        Neut_Plus = pi_N_1;
        Char_Minus = pi_C_0;
        Neut_Minus = pi_N_0;
    } else {
        return -10;
    }


    TLorentzVector Neut_Plus_Smear;
    TLorentzVector Neut_Minus_Smear;


    if (smear_plus == 1) {

        Neut_Plus_Smear = Smear_The_Neutral_Pion_Plus_para(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Plus_para(Char_Minus, Neut_Minus);

    } else if (smear_plus == -1) {
        Neut_Plus_Smear = Smear_The_Neutral_Pion_Minus_para(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Minus_para(Char_Minus, Neut_Minus);
    }


    float phi_star_CP = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus_Smear, Char_Minus, Neut_Minus_Smear,
                                            Reference_Frame_Tautau);


    // Upsilon separation



    float y0y1 = Cal_rhorho_Upsilon(Char_Plus, Neut_Plus, Reference_Frame_Tautau) *
                 Cal_rhorho_Upsilon(Char_Minus, Neut_Minus, Reference_Frame_Tautau);

    if (y0y1 < 0) {
        if (phi_star_CP > TMath::Pi())
            phi_star_CP -= TMath::Pi();
        else
            phi_star_CP += TMath::Pi();
    }

    return phi_star_CP;


}


float Calculated_Phi_Star_rhorho_Smearing_perp(const TLorentzVector pi_C_0, const TLorentzVector pi_N_0,
                                               const TLorentzVector pi_C_1, const TLorentzVector pi_N_1,
                                               float charge_tau_0, int smear_plus) {


    TLorentzVector Reference_Frame_Tautau = pi_C_0 + pi_N_0 + pi_C_1 + pi_N_1;
//    float Upsilon_rho = Upsilon(OtherTau_Charged,OtherTau_Neutral,Reference_Frame_Tautau);







    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;

    if (charge_tau_0 > 0.1) {

        Char_Plus = pi_C_0;
        Neut_Plus = pi_N_0;
        Char_Minus = pi_C_1;
        Neut_Minus = pi_N_1;


    } else if (charge_tau_0 < -0.1) {
        Char_Plus = pi_C_1;
        Neut_Plus = pi_N_1;
        Char_Minus = pi_C_0;
        Neut_Minus = pi_N_0;
    } else {
        return -10;
    }


    TLorentzVector Neut_Plus_Smear;
    TLorentzVector Neut_Minus_Smear;


    if (smear_plus == 1) {

        Neut_Plus_Smear = Smear_The_Neutral_Pion_Plus_perp(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Plus_perp(Char_Minus, Neut_Minus);

    } else if (smear_plus == -1) {
        Neut_Plus_Smear = Smear_The_Neutral_Pion_Minus_perp(Char_Plus, Neut_Plus);
        Neut_Minus_Smear = Smear_The_Neutral_Pion_Minus_perp(Char_Minus, Neut_Minus);
    }


    float phi_star_CP = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus_Smear, Char_Minus, Neut_Minus_Smear,
                                            Reference_Frame_Tautau);


    // Upsilon separation



    float y0y1 = Cal_rhorho_Upsilon(Char_Plus, Neut_Plus, Reference_Frame_Tautau) *
                 Cal_rhorho_Upsilon(Char_Minus, Neut_Minus, Reference_Frame_Tautau);

    if (y0y1 < 0) {
        if (phi_star_CP > TMath::Pi())
            phi_star_CP -= TMath::Pi();
        else
            phi_star_CP += TMath::Pi();
    }

    return phi_star_CP;


}


float Calculated_Phi_Star_a1_with_highest_pT(const TLorentzVector Track0, const TLorentzVector Track1,
                                             const TLorentzVector Track2, const TLorentzVector OtherTau_Charged,
                                             const TLorentzVector OtherTau_Neutral, float charge_3p) {


    TLorentzVector intermediate_rho;  // the intermediate rho^0 (a1 -> pi rho)
    TLorentzVector pion;              // the pion
    float rhomass = 775.26;
    float best_mass_diff = 999;

//    std::vector<std::pair<int, int> > combs = {std::pair<int, int>(0, 1), std::pair<int, int>(0, 2), std::pair<int, int>(1, 2)};
//    for (auto comb : combs) {
//        TLorentzVector rho = a1tracks.at(comb.first) + a1tracks.at(comb.second);
//        float massdiff = fabs(rho.M() - rhomass);
//        if (massdiff < best_mass_diff) {
//            intermediate_rho = rho;
//            pion = a1tracks.at(3 - comb.first - comb.second);
//        }
//    }
    TLorentzVector rho_01 = Track0 + Track1;
    TLorentzVector rho_02 = Track0 + Track2;
    TLorentzVector rho_12 = Track1 + Track2;


    intermediate_rho = rho_01;
    pion = Track2;


    TLorentzVector Reference_Frame_Tautau = pion + intermediate_rho + OtherTau_Charged + OtherTau_Neutral;
    float Upsilon_rho = Upsilon(OtherTau_Charged, OtherTau_Neutral, Reference_Frame_Tautau);


    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;
    float Phi_Star_a1_rho_wrong;

    if (charge_3p > 0.1) {

        Char_Plus = pion;
        Neut_Plus = intermediate_rho;
        Char_Minus = OtherTau_Charged;
        Neut_Minus = OtherTau_Neutral;


    } else if (charge_3p < -0.1) {
        Char_Plus = OtherTau_Charged;
        Neut_Plus = OtherTau_Neutral;
        Char_Minus = pion;
        Neut_Minus = intermediate_rho;
    } else {
        return -10;
    }


    Phi_Star_a1_rho_wrong = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus, Char_Minus, Neut_Minus, Reference_Frame_Tautau);


    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);


    if (Phi_Star_a1_rho_wrong < TMath::Pi())
        Phi_Star_a1_rho_wrong += TMath::Pi();
    else
        Phi_Star_a1_rho_wrong -= TMath::Pi();

    //Why?

    if (y_a1 * Upsilon_rho > 0) { /* OK */
    } else {
        // Add +/- pi
        if (Phi_Star_a1_rho_wrong < TMath::Pi())
            Phi_Star_a1_rho_wrong += TMath::Pi();
        else
            Phi_Star_a1_rho_wrong -= TMath::Pi();
    }
//flip twice





    return Phi_Star_a1_rho_wrong;
}

float
Calculated_Phi_Star_a1_wrong(const TLorentzVector Track0, const TLorentzVector Track1, const TLorentzVector Track2,
                             const TLorentzVector OtherTau_Charged, const TLorentzVector OtherTau_Neutral,
                             float charge_3p) {


    TLorentzVector intermediate_rho;  // the intermediate rho^0 (a1 -> pi rho)
    TLorentzVector pion;              // the pion
    float rhomass = 775.26;
    float best_mass_diff = 999;

//    std::vector<std::pair<int, int> > combs = {std::pair<int, int>(0, 1), std::pair<int, int>(0, 2), std::pair<int, int>(1, 2)};
//    for (auto comb : combs) {
//        TLorentzVector rho = a1tracks.at(comb.first) + a1tracks.at(comb.second);
//        float massdiff = fabs(rho.M() - rhomass);
//        if (massdiff < best_mass_diff) {
//            intermediate_rho = rho;
//            pion = a1tracks.at(3 - comb.first - comb.second);
//        }
//    }
    TLorentzVector rho_01 = Track0 + Track1;
    TLorentzVector rho_02 = Track0 + Track2;
    TLorentzVector rho_12 = Track1 + Track2;

    if ((fabs(rho_01.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_01;
        pion = Track2;
//        best_mass_diff = fabs(rho_01.M() - rhomass);
    }
    if ((fabs(rho_02.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_02;
        pion = Track1;
//        best_mass_diff = fabs(rho_02.M() - rhomass);
    }
    if ((fabs(rho_12.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_12;
        pion = Track0;
//        best_mass_diff = fabs(rho_12.M() - rhomass);
    }

    TLorentzVector Reference_Frame_Tautau = pion + intermediate_rho + OtherTau_Charged + OtherTau_Neutral;
    float Upsilon_rho = Upsilon(OtherTau_Charged, OtherTau_Neutral, Reference_Frame_Tautau);


    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;
    float Phi_Star_a1_rho_wrong;

    if (charge_3p > 0.1) {

        Char_Plus = pion;
        Neut_Plus = intermediate_rho;
        Char_Minus = OtherTau_Charged;
        Neut_Minus = OtherTau_Neutral;


    } else if (charge_3p < -0.1) {
        Char_Plus = OtherTau_Charged;
        Neut_Plus = OtherTau_Neutral;
        Char_Minus = pion;
        Neut_Minus = intermediate_rho;
    } else {
        return -10;
    }


    Phi_Star_a1_rho_wrong = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus, Char_Minus, Neut_Minus, Reference_Frame_Tautau);


    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);


    if (Phi_Star_a1_rho_wrong < TMath::Pi())
        Phi_Star_a1_rho_wrong += TMath::Pi();
    else
        Phi_Star_a1_rho_wrong -= TMath::Pi();

    //Why?

    if (y_a1 * Upsilon_rho > 0) { /* OK */
    } else {
        // Add +/- pi
        if (Phi_Star_a1_rho_wrong < TMath::Pi())
            Phi_Star_a1_rho_wrong += TMath::Pi();
        else
            Phi_Star_a1_rho_wrong -= TMath::Pi();
    }
//flip twice





    return Phi_Star_a1_rho_wrong;
}


float Calculated_Phi_Star_a1(const TLorentzVector Track0, const TLorentzVector Track1, const TLorentzVector Track2,
                             const TLorentzVector OtherTau_Charged, const TLorentzVector OtherTau_Neutral,
                             float charge_3p) {


    TLorentzVector intermediate_rho;  // the intermediate rho^0 (a1 -> pi rho)
    TLorentzVector pion;              // the pion
    float rhomass = 0.00015;
    float best_mass_diff = 999;

//    std::vector<std::pair<int, int> > combs = {std::pair<int, int>(0, 1), std::pair<int, int>(0, 2), std::pair<int, int>(1, 2)};
//    for (auto comb : combs) {
//        TLorentzVector rho = a1tracks.at(comb.first) + a1tracks.at(comb.second);
//        float massdiff = fabs(rho.M() - rhomass);
//        if (massdiff < best_mass_diff) {
//            intermediate_rho = rho;
//            pion = a1tracks.at(3 - comb.first - comb.second);
//        }
//    }
    TLorentzVector rho_01 = Track0 + Track1;
    TLorentzVector rho_02 = Track0 + Track2;
    TLorentzVector rho_12 = Track1 + Track2;

    if ((fabs(rho_01.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_01;
        pion = Track2;
        best_mass_diff = fabs(rho_01.M() - rhomass);
    }
    if ((fabs(rho_02.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_02;
        pion = Track1;
        best_mass_diff = fabs(rho_02.M() - rhomass);
    }
    if ((fabs(rho_12.M() - rhomass)) < best_mass_diff) {
        intermediate_rho = rho_12;
        pion = Track0;
        best_mass_diff = fabs(rho_12.M() - rhomass);
    }

    TLorentzVector Reference_Frame_Tautau = pion + intermediate_rho + OtherTau_Charged + OtherTau_Neutral;
    float Upsilon_rho = Upsilon(OtherTau_Charged, OtherTau_Neutral, Reference_Frame_Tautau);


    TLorentzVector Char_Plus;
    TLorentzVector Neut_Plus;
    TLorentzVector Char_Minus;
    TLorentzVector Neut_Minus;
    float Phi_Star_a1_rho_correct;

    if (charge_3p > 0.1) {

        Char_Plus = pion;
        Neut_Plus = intermediate_rho;
        Char_Minus = OtherTau_Charged;
        Neut_Minus = OtherTau_Neutral;


    } else if (charge_3p < -0.1) {
        Char_Plus = OtherTau_Charged;
        Neut_Plus = OtherTau_Neutral;
        Char_Minus = pion;
        Neut_Minus = intermediate_rho;
    } else {
        return -10;
    }


    Phi_Star_a1_rho_correct = Cal_phi_Star_RhoRho(Char_Plus, Neut_Plus, Char_Minus, Neut_Minus, Reference_Frame_Tautau);


    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);


    if (Phi_Star_a1_rho_correct < TMath::Pi())
        Phi_Star_a1_rho_correct += TMath::Pi();
    else
        Phi_Star_a1_rho_correct -= TMath::Pi();

    //Why?

    if (y_a1 * Upsilon_rho > 0) { /* OK */
    } else {
        // Add +/- pi
        if (Phi_Star_a1_rho_correct < TMath::Pi())
            Phi_Star_a1_rho_correct += TMath::Pi();
        else
            Phi_Star_a1_rho_correct -= TMath::Pi();
    }
//flip twice





    return Phi_Star_a1_rho_correct;
}


void Cal_Phi_Star_rhorho_smearing() {


    TChain *chain_Calculate = new TChain("NOMINAL");
//    chain0->Add("../Tau-VBFSR/user.mihuebne.Htt_hh_V02.mc16_13TeV.345123.PoPy8_NNLOPS_nnlo_30_ggH125_tautauh30h20.D3.e5814_s3126_r9364_p3749.smPre_w_0_HS/*.root");


//VBF
    for (const auto &filename : glob(
        "./../HadHad_Samples/unpol/nom/mc16e/group.phys-higgs.Htt_hh_V02.mc16_13TeV.346573.PoPy8_NNPDF30_VBFH125_tth30h20_unpol.D3.e7639_s3126_r10724_p3759.smPre_w_1_HS/*.root")) {
        chain_Calculate->Add(filename.c_str());
    }

//////VBF_Original
//    for (const auto &filename : glob(
//        "../../Output_files/4_files_Large_Data/Original_pT_based/*.root")) {
//        chain_Calculate->Add(filename.c_str());
//    }

//
//////VBF_Mass_Based
//    for (const auto &filename : glob(
//        "../../Output_files/4_files_Large_Data/Mass_Based/*.root")) {
//        chain_Calculate->Add(filename.c_str());
//    }

//////VBF_pT_Based_highest_as_pion
//    for (const auto &filename : glob(
//        "../../Output_files/4_files_Large_Data/fixed_pT_based_highest_as_pion/*.root")) {
//        chain_Calculate->Add(filename.c_str());
//    }




    UInt_t is_dijet_centrality;
    Float_t ditau_mmc_mlm_m;
    Double_t tau_0_leadTrk_d0_sig;
    Double_t tau_1_leadTrk_d0_sig;
    Float_t ditau_CP_tau0_upsilon;
    Float_t ditau_CP_tau1_upsilon;
    Float_t ditau_matched_CP_tau0_upsilon;
    Float_t ditau_matched_CP_tau1_upsilon;
    UInt_t tau_0_decay_mode, tau_1_decay_mode;
    Float_t ditau_CP_phi_star_cp_a1_rho;
    Float_t ditau_CP_phi_star_cp_ip_ip;
    Float_t ditau_CP_phi_star_cp_ip_rho;
    Float_t ditau_CP_phi_star_cp_ip_rho_opt;
    Float_t ditau_CP_phi_star_cp_rho_ip;
    Float_t ditau_CP_phi_star_cp_rho_rho;
    Float_t ditau_matched_CP_phi_star_cp_a1_rho;
    Float_t ditau_matched_CP_phi_star_cp_ip_ip;
    Float_t ditau_matched_CP_phi_star_cp_ip_rho;
    Float_t ditau_matched_CP_phi_star_cp_ip_rho_opt;
    Float_t ditau_matched_CP_phi_star_cp_rho_ip;
    Float_t ditau_matched_CP_phi_star_cp_rho_rho;
    Double_t tauspinner_HCP_Theta_0;
    Double_t tauspinner_HCP_Theta_10;
    Double_t tauspinner_HCP_Theta_100;
    Double_t tauspinner_HCP_Theta_110;
    Double_t tauspinner_HCP_Theta_120;
    Double_t tauspinner_HCP_Theta_130;
    Double_t tauspinner_HCP_Theta_140;
    Double_t tauspinner_HCP_Theta_150;
    Double_t tauspinner_HCP_Theta_160;
    Double_t tauspinner_HCP_Theta_170;
    Double_t tauspinner_HCP_Theta_20;
    Double_t tauspinner_HCP_Theta_30;
    Double_t tauspinner_HCP_Theta_40;
    Double_t tauspinner_HCP_Theta_50;
    Double_t tauspinner_HCP_Theta_60;
    Double_t tauspinner_HCP_Theta_70;
    Double_t tauspinner_HCP_Theta_80;
    Double_t tauspinner_HCP_Theta_90;
    Int_t tauspinner_helicity;
    Double_t weight_mc;
    Float_t ditau_dr;
    Double_t ditau_higgspt;
    Float_t theory_weights_nominal;

    TLorentzVector *tau_0_matched_p4_vis_charged_track0 = new TLorentzVector;
    TLorentzVector *tau_0_matched_p4_vis_charged_track1 = new TLorentzVector;
    TLorentzVector *tau_0_matched_p4_vis_charged_track2 = new TLorentzVector;
    TLorentzVector *tau_1_matched_p4_vis_charged_track0 = new TLorentzVector;
    TLorentzVector *tau_1_matched_p4_vis_charged_track1 = new TLorentzVector;
    TLorentzVector *tau_1_matched_p4_vis_charged_track2 = new TLorentzVector;
    TLorentzVector *tau_0_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4 = new TLorentzVector;
    TLorentzVector *tau_1_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_1_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_1_track2_p4 = new TLorentzVector;
    TLorentzVector *tau_0_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_0_decay_neutral_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4 = new TLorentzVector;

    UInt_t tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM; //Float_T?
    UInt_t tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    UInt_t tau_0_n_charged_tracks;
    UInt_t tau_1_n_charged_tracks;
    Float_t tau_0_q;
    Float_t tau_1_q;
    Float_t ditau_qxq;


    chain_Calculate->SetBranchAddress("ditau_mmc_mlm_m", &ditau_mmc_mlm_m);
    chain_Calculate->SetBranchAddress("tau_0_leadTrk_d0_sig", &tau_0_leadTrk_d0_sig);
    chain_Calculate->SetBranchAddress("tau_1_leadTrk_d0_sig", &tau_1_leadTrk_d0_sig);
    chain_Calculate->SetBranchAddress("ditau_CP_tau0_upsilon", &ditau_CP_tau0_upsilon);
    chain_Calculate->SetBranchAddress("ditau_CP_tau1_upsilon", &ditau_CP_tau1_upsilon);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_tau0_upsilon", &ditau_matched_CP_tau0_upsilon);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_tau1_upsilon", &ditau_matched_CP_tau1_upsilon);
    chain_Calculate->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode);
    chain_Calculate->SetBranchAddress("tau_1_decay_mode", &tau_1_decay_mode);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_a1_rho", &ditau_CP_phi_star_cp_a1_rho);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_ip_ip", &ditau_CP_phi_star_cp_ip_ip);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_ip_rho", &ditau_CP_phi_star_cp_ip_rho);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_ip_rho_opt", &ditau_CP_phi_star_cp_ip_rho_opt);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_rho_ip", &ditau_CP_phi_star_cp_rho_ip);
    chain_Calculate->SetBranchAddress("ditau_CP_phi_star_cp_rho_rho", &ditau_CP_phi_star_cp_rho_rho);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_a1_rho", &ditau_matched_CP_phi_star_cp_a1_rho);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_ip_ip", &ditau_matched_CP_phi_star_cp_ip_ip);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_ip_rho", &ditau_matched_CP_phi_star_cp_ip_rho);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_ip_rho_opt",
                                      &ditau_matched_CP_phi_star_cp_ip_rho_opt);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_rho_ip", &ditau_matched_CP_phi_star_cp_rho_ip);
    chain_Calculate->SetBranchAddress("ditau_matched_CP_phi_star_cp_rho_rho", &ditau_matched_CP_phi_star_cp_rho_rho);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_0", &tauspinner_HCP_Theta_0);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_10", &tauspinner_HCP_Theta_10);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_100", &tauspinner_HCP_Theta_100);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_110", &tauspinner_HCP_Theta_110);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_120", &tauspinner_HCP_Theta_120);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_130", &tauspinner_HCP_Theta_130);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_140", &tauspinner_HCP_Theta_140);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_150", &tauspinner_HCP_Theta_150);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_160", &tauspinner_HCP_Theta_160);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_170", &tauspinner_HCP_Theta_170);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_20", &tauspinner_HCP_Theta_20);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_30", &tauspinner_HCP_Theta_30);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_40", &tauspinner_HCP_Theta_40);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_50", &tauspinner_HCP_Theta_50);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_60", &tauspinner_HCP_Theta_60);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_70", &tauspinner_HCP_Theta_70);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_80", &tauspinner_HCP_Theta_80);
    chain_Calculate->SetBranchAddress("tauspinner_HCP_Theta_90", &tauspinner_HCP_Theta_90);
    chain_Calculate->SetBranchAddress("weight_mc", &weight_mc);
    chain_Calculate->SetBranchAddress("theory_weights_nominal", &theory_weights_nominal);
    chain_Calculate->SetBranchAddress("tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                      &tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
    chain_Calculate->SetBranchAddress("tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                      &tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
    chain_Calculate->SetBranchAddress("tau_0_n_charged_tracks", &tau_0_n_charged_tracks);
    chain_Calculate->SetBranchAddress("tau_1_n_charged_tracks", &tau_1_n_charged_tracks);
    chain_Calculate->SetBranchAddress("tau_0_q", &tau_0_q);
    chain_Calculate->SetBranchAddress("tau_1_q", &tau_1_q);
    chain_Calculate->SetBranchAddress("ditau_qxq", &ditau_qxq);
    chain_Calculate->SetBranchAddress("tau_0_matched_p4_vis_charged_track0", &tau_0_matched_p4_vis_charged_track0);
    chain_Calculate->SetBranchAddress("tau_0_matched_p4_vis_charged_track1", &tau_0_matched_p4_vis_charged_track1);
    chain_Calculate->SetBranchAddress("tau_0_matched_p4_vis_charged_track2", &tau_0_matched_p4_vis_charged_track2);
    chain_Calculate->SetBranchAddress("tau_1_matched_p4_vis_charged_track0", &tau_0_matched_p4_vis_charged_track0);
    chain_Calculate->SetBranchAddress("tau_1_matched_p4_vis_charged_track1", &tau_0_matched_p4_vis_charged_track1);
    chain_Calculate->SetBranchAddress("tau_1_matched_p4_vis_charged_track2", &tau_0_matched_p4_vis_charged_track2);
    chain_Calculate->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4);
    chain_Calculate->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4);
    chain_Calculate->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4);
    chain_Calculate->SetBranchAddress("tau_1_track0_p4", &tau_1_track0_p4);
    chain_Calculate->SetBranchAddress("tau_1_track1_p4", &tau_1_track1_p4);
    chain_Calculate->SetBranchAddress("tau_1_track2_p4", &tau_1_track2_p4);
    chain_Calculate->SetBranchAddress("tau_0_decay_charged_p4", &tau_0_decay_charged_p4);
    chain_Calculate->SetBranchAddress("tau_0_decay_neutral_p4", &tau_0_decay_neutral_p4);
    chain_Calculate->SetBranchAddress("tau_1_decay_charged_p4", &tau_1_decay_charged_p4);
    chain_Calculate->SetBranchAddress("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4);

    TCanvas *c1 = new TCanvas("c1", "c1", 600, 600);
    TH1F *Phi_star_Even = new TH1F("Phi_star_Even", "phi_star_CP_Even", 5, 0, 6.3);
    TH1F *Phi_star_Even_Smear = new TH1F("Phi_star_Even_Smear", "Phi_star_Even_Smear", 5, 0, 6.3);
    TH1F *Phi_star_Odd = new TH1F("Phi_star_Odd", "phi_star_CP_Odd", 5, 0, 6.3);
    TH1F *Phi_star_Even_Ratio = new TH1F("Phi_star_Even_Ratio", "Phi_star_Even_Ratio", 5, 0, 6.3);

    float Even_Weight = 0;
    float Odd_Weight = 0;
    float Even_Weight_Smear = 0;

    Int_t nentries = (Int_t) chain_Calculate->GetEntries();
    for (Int_t i = 0; i < nentries; i++) {
        chain_Calculate->GetEntry(i);

        if (tau_0_decay_mode == 1 && tau_1_decay_mode == 1 &&
            Preselection_Check(tau_0_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                               tau_1_trig_HLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                               tau_0_n_charged_tracks, tau_1_n_charged_tracks, tau_0_q, tau_1_q, ditau_qxq) == 1) {

//
//            Phi_star_Even->Fill(ditau_CP_phi_star_cp_a1_rho,tauspinner_HCP_Theta_0);
//            Phi_star_Odd->Fill(ditau_CP_phi_star_cp_a1_rho,tauspinner_HCP_Theta_90);

//            float Wrong_phi_Star = Calculated_Phi_Star_a1_wrong(*tau_1_track0_p4,*tau_1_track1_p4,*tau_1_track2_p4,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,tau_1_q);
//
//            Phi_star_Even->Fill(Wrong_phi_Star,tauspinner_HCP_Theta_0);
//            Phi_star_Odd->Fill(Wrong_phi_Star,tauspinner_HCP_Theta_90);

//            float Correct_phi_Star = Calculated_Phi_Star_a1(*tau_1_track0_p4,*tau_1_track1_p4,*tau_1_track2_p4,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,tau_1_q);
//
//            Phi_star_Even->Fill(Correct_phi_Star,tauspinner_HCP_Theta_0);
//            Phi_star_Odd->Fill(Correct_phi_Star,tauspinner_HCP_Theta_90);
//
////
//            float Truth_Matched_phi_Star = ditau_matched_CP_phi_star_cp_a1_rho;
//
//            Phi_star_Even->Fill(ditau_matched_CP_phi_star_cp_a1_rho,tauspinner_HCP_Theta_0);
//            Phi_star_Odd->Fill(ditau_matched_CP_phi_star_cp_a1_rho,tauspinner_HCP_Theta_90);

//            float High_pT_phi_Star = Calculated_Phi_Star_a1_with_highest_pT(*tau_1_track0_p4,*tau_1_track1_p4,*tau_1_track2_p4,*tau_0_decay_charged_p4,*tau_0_decay_neutral_p4,tau_1_q);
//


//            float phi_Star_rhorho = ditau_CP_phi_star_cp_rho_rho;

            float phi_Star_rhorho = Calculated_Phi_Star_rhorho_Original(*tau_0_decay_charged_p4,
                                                                        *tau_0_decay_neutral_p4,
                                                                        *tau_1_decay_charged_p4,
                                                                        *tau_1_decay_neutral_p4, tau_0_q);

//            int Smear_plus = 1;
            int Smear_plus = -1;

//            float phi_Star_rhorho_Smear = Calculated_Phi_Star_rhorho_Smearing_para(*tau_0_decay_charged_p4, *tau_0_decay_neutral_p4, *tau_1_decay_charged_p4, *tau_1_decay_neutral_p4,tau_0_q,Smear_plus);


            float phi_Star_rhorho_Smear = Calculated_Phi_Star_rhorho_Smearing(*tau_0_decay_charged_p4,
                                                                              *tau_0_decay_neutral_p4,
                                                                              *tau_1_decay_charged_p4,
                                                                              *tau_1_decay_neutral_p4, tau_0_q,
                                                                              Smear_plus);


            Phi_star_Even->Fill(phi_Star_rhorho, tauspinner_HCP_Theta_0);
            Phi_star_Even_Smear->Fill(phi_Star_rhorho_Smear, tauspinner_HCP_Theta_0);
//            Phi_star_Odd->Fill(phi_Star_rhorho,tauspinner_HCP_Theta_90);



            if (phi_Star_rhorho > 0 && phi_Star_rhorho < 6.3) {
                Even_Weight += tauspinner_HCP_Theta_0;
//                Odd_Weight +=tauspinner_HCP_Theta_90;
            }

            if (phi_Star_rhorho_Smear > 0 && phi_Star_rhorho_Smear < 6.3) {
                Even_Weight_Smear += tauspinner_HCP_Theta_0;
//                Odd_Weight +=tauspinner_HCP_Theta_90;
            }


        }


    }


    TLegend *legend = new TLegend(0.0, 0.9, 0.2, 1.0);
    legend->AddEntry(Phi_star_Even, "CP Even", "l");
    legend->AddEntry(Phi_star_Odd, "CP Odd", "l");
    Phi_star_Even_Smear->SetLineColor(2);
    Phi_star_Even->GetXaxis()->SetTitle("Phi_star_CP");
    Phi_star_Even->GetYaxis()->SetTitle("Normalized Entries      ");
    Phi_star_Even->SetTitle("Test_phi_star_CP_rhorho_Smear_Minus_para.png");
    Double_t norm = Phi_star_Even->GetEntries();
    Phi_star_Even->Scale(1 / Even_Weight);
    Phi_star_Even_Smear->Scale(1 / Even_Weight_Smear);
    Phi_star_Even->GetYaxis()->SetRangeUser(0.15, 0.25);


    for (int i_bin_count = 1; i_bin_count < 6; i_bin_count++) {

        if (Phi_star_Even_Smear->GetBinContent(i_bin_count) > 0 && Phi_star_Even->GetBinContent(i_bin_count) > 0) {
            Phi_star_Even_Ratio->SetBinContent(i_bin_count, Phi_star_Even_Smear->GetBinContent(i_bin_count) /
                                                            Phi_star_Even->GetBinContent(i_bin_count));
            double err = sqrt(
                pow(Phi_star_Even_Smear->GetBinError(i_bin_count) / Phi_star_Even->GetBinContent(i_bin_count), 2.0) +
                pow(Phi_star_Even->GetBinError(i_bin_count) / Phi_star_Even_Smear->GetBinContent(i_bin_count), 2.0));
            Phi_star_Even_Ratio->SetBinError(i_bin_count, err);
        }
    }

    Phi_star_Even_Ratio->GetYaxis()->SetRangeUser(0.9, 1.1);

    c1->cd(1);
    Phi_star_Even->Draw();
    Phi_star_Even_Smear->Draw("same");

    legend->Draw();

    c1->cd(2);
    Phi_star_Even_Ratio->Draw("e");

    TString Final_Name = "./Plots/Test_phi_star_CP_rhorho_Smear_Minus_para.png";
    const char *Final_File_Name = Final_Name.Data();
    c1->Print(Final_File_Name);
    c1->Close();
    delete c1;

    return;
}

