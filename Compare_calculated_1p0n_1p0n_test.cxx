#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"
#include <chrono>
#include <glob.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include "AtlasLabels.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"

//using namespace TMVA;
using namespace std;
using namespace chrono;

//bool sortcol( const vector<float>& v1,const vector<float>& v2 ) {
//    return v1[0] > v2[0];
//}



vector <string> glob(const char *pattern) {
    glob_t g;
    glob(pattern, GLOB_TILDE, nullptr, &g); // one should ensure glob returns 0!
    vector <string> filelist;
    filelist.reserve(g.gl_pathc);
    for (size_t i = 0; i < g.gl_pathc; ++i) {
        filelist.emplace_back(g.gl_pathv[i]);
    }
    globfree(&g);
    return filelist;
}


void myText(Double_t x,Double_t y,Color_t color, const char *text) {

    Double_t tsize=0.03;
    TLatex l; //l.SetTextAlign(12);
    l.SetTextSize(tsize);
    l.SetNDC();
    l.SetTextColor(color);
    l.DrawLatex(x,y,text);
}

void myMarkerText(Double_t x, Double_t y, Int_t color, Int_t mstyle, const char *text, Float_t msize) {
    Double_t tsize = 0.06;
    TMarker *marker = new TMarker(x - (0.4 * tsize), y, 8);
    marker->SetMarkerColor(color);
    marker->SetNDC();
    marker->SetMarkerStyle(mstyle);
    marker->SetMarkerSize(msize);
    marker->Draw();

    TLatex l;
    l.SetTextAlign(12); //l.SetTextSize(tsize);
    l.SetTextSize(0.03);
    l.SetNDC();
    l.DrawLatex(x, y, text);
}

void ATLASLabel(Double_t x, Double_t y, const char *text, Color_t color) {
    TLatex l; //l.SetTextAlign(12); l.SetTextSize(tsize);
    l.SetNDC();
    l.SetTextFont(72);
    l.SetTextColor(color);

    double delx = 0.115 * 696 * gPad->GetWh() / (472 * gPad->GetWw());

    l.DrawLatex(x, y, "ATLAS");
    if (text) {
        TLatex p;
        p.SetNDC();
        p.SetTextFont(42);
        p.SetTextColor(color);
        p.DrawLatex(x + delx, y, text);
        //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
    }
}

TVector3
CalculateImpactParamVec(float T_d0, float T_z0, float T_VX, float T_VY, float T_VZ, float T_Phi, float PV_X, float PV_Y,
                        float PV_Z) {

    TVector3 pionVector;
    TVector3 initialPoint;
    TVector3 primaryVertex;
    TVector3 IPVector;


    pionVector.SetXYZ(T_VX, T_VY, T_VZ);
    initialPoint.SetXYZ(-T_d0 * sin(T_Phi), T_d0 * cos(T_Phi), T_z0);
    primaryVertex.SetXYZ(PV_X, PV_Y, PV_Z);
    double k = (primaryVertex - initialPoint) * pionVector / pionVector.Mag2();
    IPVector = initialPoint + k * pionVector - primaryVertex;

    return IPVector;


}


TVector3
CalculateImpactParamVec_Truth(float Truth_prod_vtx_x, float Truth_prod_vtx_y, float Truth_prod_vtx_z,
                              float Truth_dec_vtx_x, float Truth_dec_vtx_y, float Truth_dec_vtx_z,
                              const TLorentzVector &Decay_Charged) {

    TVector3 pionVector;
    TVector3 initialPoint;
    TVector3 primaryVertex;
    TVector3 IPVector;


    pionVector = Decay_Charged.Vect();
    initialPoint.SetXYZ(Truth_dec_vtx_x, Truth_dec_vtx_y, Truth_dec_vtx_z);
    primaryVertex.SetXYZ(Truth_prod_vtx_x, Truth_prod_vtx_y, Truth_prod_vtx_z);
    double k = (primaryVertex - initialPoint) * pionVector / pionVector.Mag2();
    IPVector = initialPoint + k * pionVector - primaryVertex;

    return IPVector;


}


float Calculate_angle_between_TVectors(const TVector3 &TV_0, const TVector3 &TV_1) {


    float Angle = TV_0.Angle(TV_1);
    return Angle;


}

int Check_Loose_charge_Only(float Charge_Product) {

    if (Charge_Product < 0) {
        return 1;
    } else {
        return 0;
    }

}


float Cal_1p1p_Upsilon(TLorentzVector charged, TLorentzVector neutral, const TLorentzVector &referenceFrame) {
    // only boost if we can boost into the reference frame
    if (referenceFrame.E() != 0.) {
        TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
        charged.Boost(boostIntoReferenceFrame);
        neutral.Boost(boostIntoReferenceFrame);
    }
    return (charged.E() - neutral.E()) / (charged.E() + neutral.E());
}

float Cal_xTauFW_Upsilon_a1(TLorentzVector pion, TLorentzVector intermediate_rho) {
    float a1mass2 = (intermediate_rho + pion).M2();
    float y_a1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) -
                 (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);

    return y_a1;

}

float Cal_xTauFW_Upsilon_a1_new(TLorentzVector pion, TLorentzVector intermediate_rho) {


    float a1mass2 = (intermediate_rho + pion).M2();

    float massterm = (a1mass2 - pion.M2() + intermediate_rho.M2()) / (2 * a1mass2);
    float y1_v1 = (intermediate_rho.E() - pion.E()) / (intermediate_rho.E() + pion.E()) - massterm;
    float y1_v2 = (intermediate_rho.E()) / (intermediate_rho.E() + pion.E());
    if (y1_v2 > massterm) {
        y1_v2 = -y1_v2;
    }
    return y1_v2;


}


void Compare_two_hist(TH1F* The_hist_to_draw_Even, TH1F* The_hist_to_draw_Odd, TString File_name, TString H1_name, TString H2_name, float sum_weight_even, float sum_weight_odd){
    TString The_x_Label = File_name;
    TString File_Path = "./Plots/";
    TString File_Type = "_in_ATLAS_sample.png";
    TString The_Output_png_name = File_Path + File_name + File_Type;
    const char *Final_File_Name = The_Output_png_name.Data();

    const char *First_hist_name = H1_name.Data();
    const char *Second_hist_name = H2_name.Data();

    vector<float> Bins_Even;
    vector<float> Bins_Odd;
    Bins_Even.clear();
    Bins_Odd.clear();
    Bins_Even.push_back(The_hist_to_draw_Even->GetBinContent(1));
    Bins_Even.push_back(The_hist_to_draw_Even->GetBinContent(2));
    Bins_Even.push_back(The_hist_to_draw_Even->GetBinContent(3));
    Bins_Even.push_back(The_hist_to_draw_Even->GetBinContent(4));
    Bins_Even.push_back(The_hist_to_draw_Even->GetBinContent(5));
    Bins_Odd.push_back(The_hist_to_draw_Odd->GetBinContent(1));
    Bins_Odd.push_back(The_hist_to_draw_Odd->GetBinContent(2));
    Bins_Odd.push_back(The_hist_to_draw_Odd->GetBinContent(3));
    Bins_Odd.push_back(The_hist_to_draw_Odd->GetBinContent(4));
    Bins_Odd.push_back(The_hist_to_draw_Odd->GetBinContent(5));

    float Cross_Entropy_Between_Even_Odd = Calculate_Cross_Entropy(Bins_Odd,Bins_Even);
    float Cross_Entropy_Between_Even_Even = Calculate_Cross_Entropy(Bins_Even,Bins_Even);
    float CE_score = Cross_Entropy_Between_Even_Odd-Cross_Entropy_Between_Even_Even;
    char Cross_entropy_char[7];
    sprintf(Cross_entropy_char, "%3f", CE_score);


    TCanvas *c1 = new TCanvas("c1", "c1", 1200, 1200);
    c1->cd();

    The_hist_to_draw_Even->SetLineColor(2);
    The_hist_to_draw_Odd->SetLineColor(1);
    The_hist_to_draw_Even->SetMarkerColor(2);
    The_hist_to_draw_Odd->SetMarkerColor(1);
    The_hist_to_draw_Even->Scale((1 ) / (sum_weight_even));
    The_hist_to_draw_Odd->Scale((1 ) / (sum_weight_odd));
    The_hist_to_draw_Odd->Draw("e");
    The_hist_to_draw_Even->Draw("e same");
    The_hist_to_draw_Odd->GetXaxis()->SetTitle(The_x_Label);
    The_hist_to_draw_Odd->GetYaxis()->SetTitle(" Events");
    The_hist_to_draw_Odd->GetYaxis()->SetRangeUser(0.15, 0.25);
    The_hist_to_draw_Odd->GetYaxis()->SetTitleOffset(1.3);
    The_hist_to_draw_Odd->GetXaxis()->SetTitleOffset(0.0);

    c1->Clear();
    auto rp = new TRatioPlot(The_hist_to_draw_Odd, The_hist_to_draw_Even);

    rp->SetH1DrawOpt("HIST E");
    rp->SetH2DrawOpt("HIST E");
    rp->Draw();

    rp->GetUpperRefYaxis()->SetTitleFont(43);
    rp->GetUpperRefYaxis()->SetTitleSize(40);
    rp->GetUpperRefYaxis()->SetLabelFont(43);
    rp->GetUpperRefYaxis()->SetLabelSize(40);

    rp->GetLowerRefYaxis()->SetTitle("Odd/Even");
    rp->GetLowerRefYaxis()->SetRangeUser(0.95, 1.05);
    rp->GetLowerRefYaxis()->SetNdivisions(505);
    rp->GetLowerRefYaxis()->SetTitleSize(40);
    rp->GetLowerRefYaxis()->SetTitleFont(43);
    rp->GetLowerRefYaxis()->SetTitleOffset(1.4);
    rp->GetLowerRefYaxis()->SetLabelFont(43);
    rp->GetLowerRefYaxis()->SetLabelSize(30);

    rp->GetLowerRefXaxis()->SetTitle("");
    rp->GetLowerRefXaxis()->SetTitleSize(40);
    rp->GetLowerRefXaxis()->SetTitleFont(43);
    rp->GetLowerRefXaxis()->SetTitleOffset(1.4);
    rp->GetLowerRefXaxis()->SetLabelFont(43);
    rp->GetLowerRefXaxis()->SetLabelSize(40);
    rp->GetLowerRefXaxis()->SetLabelOffset(0.02);


    myMarkerText(0.25, 0.8, 2, 20, First_hist_name, 1.3);
    myMarkerText(0.25, 0.75, 1, 20, Second_hist_name, 1.3);
    ATLASLabel(0.25, 0.85, "Work in progress");
//    myText(0.65, 0.45,1, Cross_entropy_char);
//    myText(0.45, 0.45,1, "Cross-Entropy = ");
//    rp->GetLowerRefXaxis()->SetRangeUser(-0.5,6.9);
//    legend->Draw();
//    pad1->Draw();
//    pad1->Update();

    auto upperpad_plots = rp->GetUpperPad();
    auto lowerpad_rp = rp->GetLowerPad();
    upperpad_plots->SetLeftMargin(0.2);
//    upperpad_plots->SetBottomMargin(0.9);
    lowerpad_rp->SetBottomMargin(0.7);
//    lowerpad_rp->SetLeftMargin(0.4);





    c1->Update();
    c1->Print(Final_File_Name);
    c1->Close();


    delete c1;

    return;



}


void Draw_TH1F_upsilon(TH1F* The_hist_to_draw, TString File_name){
    TString The_x_Label = File_name;
    TString File_Path = "./Plots/";
    TString File_Type = "_in_ATLAS_sample.png";
    TString The_Output_png_name = File_Path + File_name + File_Type;
    const char *Final_File_Name = The_Output_png_name.Data();


    TCanvas *c1 = new TCanvas("c1", "c1", 1200, 1200);
    c1->cd();

    The_hist_to_draw->SetLineColor(1);
    The_hist_to_draw->SetMarkerColor(1);
//    The_hist_to_draw->Scale((1 ) / (sum_weight_odd));
    The_hist_to_draw->Draw("e");
    The_hist_to_draw->GetXaxis()->SetTitle(The_x_Label);
    The_hist_to_draw->GetYaxis()->SetTitle(" Events");
//    The_hist_to_draw->GetYaxis()->SetRangeUser(0.15, 0.25);
    The_hist_to_draw->GetYaxis()->SetTitleOffset(1.3);
    The_hist_to_draw->GetXaxis()->SetTitleOffset(0.0);


//    myMarkerText(0.25, 0.8, 2, 20, "CP Even", 1.3);
//    ATLASLabel(0.25, 0.85, "Work in progress");
//    myText(0.65, 0.45,1, Cross_entropy_char);
//    myText(0.45, 0.45,1, "Cross-Entropy = ");


    c1->Update();
    c1->Print(Final_File_Name);
    c1->Close();


    delete c1;

    return;



}






float Calculate_Acoplanarity_IP(TLorentzVector piPlus, TLorentzVector ipVectorPlus, TLorentzVector piMinus, TLorentzVector ipVectorMinus,
                            const TLorentzVector& referenceFrame) {
    TVector3 boostIntoReferenceFrame = (-1) * referenceFrame.BoostVector();
    piPlus.Boost(boostIntoReferenceFrame);
    piMinus.Boost(boostIntoReferenceFrame);
    ipVectorPlus.Boost(boostIntoReferenceFrame);
    ipVectorMinus.Boost(boostIntoReferenceFrame);
    TVector3 ip3VectorPlus = ipVectorPlus.Vect();
    TVector3 ip3VectorMinus = ipVectorMinus.Vect();
    // parallel projections of impact vector onto pion direction
    TVector3 ipVectorParallelPlus = (ip3VectorPlus * piPlus.Vect().Unit()) * piPlus.Vect().Unit();
    TVector3 ipVectorParallelMinus = (ip3VectorMinus * piMinus.Vect().Unit()) * piMinus.Vect().Unit();
    // perpendicular component of impact vector
    TVector3 ipVectorPerpPlus = (ip3VectorPlus - ipVectorParallelPlus).Unit();
    TVector3 ipVectorPerpMinus = (ip3VectorMinus - ipVectorParallelMinus).Unit();
    // Triple-odd correlation
    float triplecorr = piMinus.Vect().Unit().Dot(ipVectorPerpPlus.Cross(ipVectorPerpMinus));

    float phistar = TMath::ACos(ipVectorPerpPlus * ipVectorPerpMinus);
    if (triplecorr < 0) { phistar = TMath::TwoPi() - phistar; }




    return phistar;
}




int Cal_Phi_star_1p_1p() {

    TChain *chain_Calculate_MC = new TChain("NOMINAL");
    TString Final_Name = "./Plots/";
    SetAtlasStyle();



    for (const auto &filename : glob(
        "/eos/home-j/jxiang/Test_New_Branches/Temp_20210519/HH/*/*.root")) {
        chain_Calculate_MC->Add(filename.c_str());
    }








    UInt_t tau_0_decay_mode;
    UInt_t tau_1_decay_mode;
    Float_t ditau_qxq;
    Float_t tau_0_q;
    Float_t tau_1_q;
    Float_t tau_1_trk_d0;
    Float_t tau_1_trk_z0;
    Int_t tau_1_matched_pdgId;
//    TVector3 *beamSpot_v = new TVector3;
    TVector3 *primary_vertex_v = new TVector3;
    TLorentzVector *tau_0_track0_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track1_p4 = new TLorentzVector;
    TLorentzVector *tau_0_track2_p4 = new TLorentzVector;
    TLorentzVector *tau_1_p4 = new TLorentzVector;
    Double_t tauspinner_HCP_Theta_0;
    Double_t tauspinner_HCP_Theta_10;
    Double_t tauspinner_HCP_Theta_100;
    Double_t tauspinner_HCP_Theta_110;
    Double_t tauspinner_HCP_Theta_120;
    Double_t tauspinner_HCP_Theta_130;
    Double_t tauspinner_HCP_Theta_140;
    Double_t tauspinner_HCP_Theta_150;
    Double_t tauspinner_HCP_Theta_160;
    Double_t tauspinner_HCP_Theta_170;
    Double_t tauspinner_HCP_Theta_20;
    Double_t tauspinner_HCP_Theta_30;
    Double_t tauspinner_HCP_Theta_40;
    Double_t tauspinner_HCP_Theta_50;
    Double_t tauspinner_HCP_Theta_60;
    Double_t tauspinner_HCP_Theta_70;
    Double_t tauspinner_HCP_Theta_80;
    Double_t tauspinner_HCP_Theta_90;

    Float_t tau_0_track0_q;
    Float_t tau_0_track1_q;
    Float_t tau_0_track2_q;


    TLorentzVector *tau_0_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_0_decay_neutral_p4 = new TLorentzVector;
    chain_Calculate_MC->SetBranchAddress("tau_0_decay_charged_p4", &tau_0_decay_charged_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_decay_neutral_p4", &tau_0_decay_neutral_p4);

    TLorentzVector *tau_1_decay_charged_p4 = new TLorentzVector;
    TLorentzVector *tau_1_decay_neutral_p4 = new TLorentzVector;
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_charged_p4", &tau_1_decay_charged_p4);
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_neutral_p4", &tau_1_decay_neutral_p4);


    Double_t tau_0_leadTrk_d0;
    Double_t tau_0_leadTrk_z0;
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_d0", &tau_0_leadTrk_d0);
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_d0", &tau_0_leadTrk_d0);

    TVector3 *tau_0_leadTrk_vertex_v = new TVector3;
    chain_Calculate_MC->SetBranchAddress("tau_0_leadTrk_vertex_v", &tau_0_leadTrk_vertex_v);

    Float_t         ditau_CP_ip_tau0_x_ip;
    Float_t         ditau_CP_ip_tau0_y_ip;
    Float_t         ditau_CP_ip_tau0_z_ip;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_x_ip", &ditau_CP_ip_tau0_x_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_y_ip", &ditau_CP_ip_tau0_y_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau0_z_ip", &ditau_CP_ip_tau0_z_ip);


    Float_t         ditau_CP_ip_tau1_x_ip;
    Float_t         ditau_CP_ip_tau1_y_ip;
    Float_t         ditau_CP_ip_tau1_z_ip;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_x_ip", &ditau_CP_ip_tau1_x_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_y_ip", &ditau_CP_ip_tau1_y_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_ip_tau1_z_ip", &ditau_CP_ip_tau1_z_ip);


    Float_t ditau_CP_phi_star_cp_ip_ip;
    Float_t ditau_CP_phi_star_cp_ip_rho;
    Float_t ditau_CP_phi_star_cp_rho_ip;
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_ip_ip", &ditau_CP_phi_star_cp_ip_ip);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_ip_rho", &ditau_CP_phi_star_cp_ip_rho);
    chain_Calculate_MC->SetBranchAddress("ditau_CP_phi_star_cp_rho_ip", &ditau_CP_phi_star_cp_rho_ip);

    chain_Calculate_MC->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode);
    chain_Calculate_MC->SetBranchAddress("tau_1_decay_mode", &tau_1_decay_mode);
    chain_Calculate_MC->SetBranchAddress("ditau_qxq", &ditau_qxq);
    chain_Calculate_MC->SetBranchAddress("tau_0_q", &tau_0_q);
    chain_Calculate_MC->SetBranchAddress("tau_1_q", &tau_1_q);

    chain_Calculate_MC->SetBranchAddress("tau_1_matched_pdgId", &tau_1_matched_pdgId);
//    chain_Calculate_MC->SetBranchAddress("beamSpot_v", &beamSpot_v);
    chain_Calculate_MC->SetBranchAddress("primary_vertex_v", &primary_vertex_v);
    chain_Calculate_MC->SetBranchAddress("tau_0_track0_p4", &tau_0_track0_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_track1_p4", &tau_0_track1_p4);
    chain_Calculate_MC->SetBranchAddress("tau_0_track2_p4", &tau_0_track2_p4);

    chain_Calculate_MC->SetBranchAddress("tau_1_p4", &tau_1_p4);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_0", &tauspinner_HCP_Theta_0);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_10", &tauspinner_HCP_Theta_10);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_100", &tauspinner_HCP_Theta_100);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_110", &tauspinner_HCP_Theta_110);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_120", &tauspinner_HCP_Theta_120);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_130", &tauspinner_HCP_Theta_130);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_140", &tauspinner_HCP_Theta_140);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_150", &tauspinner_HCP_Theta_150);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_160", &tauspinner_HCP_Theta_160);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_170", &tauspinner_HCP_Theta_170);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_20", &tauspinner_HCP_Theta_20);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_30", &tauspinner_HCP_Theta_30);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_40", &tauspinner_HCP_Theta_40);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_50", &tauspinner_HCP_Theta_50);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_60", &tauspinner_HCP_Theta_60);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_70", &tauspinner_HCP_Theta_70);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_80", &tauspinner_HCP_Theta_80);
    chain_Calculate_MC->SetBranchAddress("tauspinner_HCP_Theta_90", &tauspinner_HCP_Theta_90);


    TH1F *MC_Phi_star_HH1p_ipip_HL_Even_xTauFW = new TH1F("MC_Phi_star_HH1p_ipip_HL_Even_xTauFW", "", 5, 0, 6.3);
    TH1F *MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW = new TH1F("MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW", "", 5, 0, 6.3);

    TH1F *MC_Phi_star_HH1p_ipip_HL_Even_xTauFW_ntuple = new TH1F("MC_Phi_star_HH1p_ipip_HL_Even_xTauFW_ntuple", "", 5, 0, 6.3);
    TH1F *MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW_ntuple = new TH1F("MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW_ntuple", "", 5, 0, 6.3);




    float Even_Weight_ipip_HL = 0;
    float Odd_Weight_ipip_HL = 0;



    Int_t nentries = (Int_t) chain_Calculate_MC->GetEntries();
    for (Int_t i = 0; i < nentries; i++) {
        chain_Calculate_MC->GetEntry(i);


        if (tau_0_decay_mode == 0 &&
            Check_Loose_charge_Only(ditau_qxq) == 1 && tau_1_decay_mode == 0 ) {



            TVector3 Impact_vector_pion_0;
            Impact_vector_pion_0.SetXYZ(ditau_CP_ip_tau0_x_ip, ditau_CP_ip_tau0_y_ip, ditau_CP_ip_tau0_z_ip);
            TLorentzVector Pion_ip_0 = TLorentzVector(Impact_vector_pion_0.Unit(),0); //need normalize

            TVector3 Impact_vector_pion_1;
            Impact_vector_pion_1.SetXYZ(ditau_CP_ip_tau1_x_ip, ditau_CP_ip_tau1_y_ip, ditau_CP_ip_tau1_z_ip);
            TLorentzVector Pion_ip_1 = TLorentzVector(Impact_vector_pion_1.Unit(),0); //need normalize



            TLorentzVector Positive_track;
            TLorentzVector Positive_IP;
            TLorentzVector Negative_track;
            TLorentzVector Negative_IP;

            if(tau_0_q>0 && tau_1_q<0){
                Positive_track = *tau_0_decay_charged_p4;
                Positive_IP = Pion_ip_0;
                Negative_track = *tau_1_decay_charged_p4;
                Negative_IP = Pion_ip_1;
            }
            else if(tau_0_q<0 && tau_1_q>0){
                Positive_track = *tau_1_decay_charged_p4;
                Positive_IP = Pion_ip_1;
                Negative_track = *tau_0_decay_charged_p4;
                Negative_IP = Pion_ip_0;
            }
            else{
                cout<<"error"<<endl;
            }


            TLorentzVector reference_frame = *tau_0_decay_charged_p4 + *tau_0_decay_neutral_p4 + *tau_1_decay_charged_p4 + *tau_1_decay_neutral_p4 ;


            float phi_star_ip_ip_HL = Calculate_Acoplanarity_IP(Positive_track, Positive_IP,Negative_track, Negative_IP,reference_frame);


            MC_Phi_star_HH1p_ipip_HL_Even_xTauFW->Fill(phi_star_ip_ip_HL, tauspinner_HCP_Theta_0);
            MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW->Fill(phi_star_ip_ip_HL, tauspinner_HCP_Theta_90);


            MC_Phi_star_HH1p_ipip_HL_Even_xTauFW_ntuple->Fill(ditau_CP_phi_star_cp_ip_ip, tauspinner_HCP_Theta_0);
            MC_Phi_star_HH1p_ipip_HL_Odd_xTauFW_ntuple->Fill(ditau_CP_phi_star_cp_ip_ip, tauspinner_HCP_Theta_90);



            Even_Weight_ipip_HL += tauspinner_HCP_Theta_0;
            Odd_Weight_ipip_HL += tauspinner_HCP_Theta_90;







        }



    }

    delete chain_Calculate_MC;


//    cout << "success" << endl;
    return 0;

}


void Compare_calculated_1p0n_1p0n() {


    Cal_Phi_star_1p_1p();


    return;
}

